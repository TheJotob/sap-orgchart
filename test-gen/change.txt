Testdatengenerator (Final):

Changes:

- balancierte Baumstruktur der .json-Datei durch Weglassen der Ebenenbeschränkung
  und Umschreiben des Algorithmus für die Erzeugung der Beziehungen.
- gerichtete und ungerichtete informelle Beziehung im Baum (01.. und 00..)
  zusätzliche eine ungerichtete formelle Beziehung (10) zwischen oberster Organisation und CEO
- Bug gefixt, dass die ersten 15 informellen Beziehungen immer generiert wurden. 


Changes:

- minimierte Größe der json-Datei
- relationship_type besteht nun aus 4 binären Characters z.B. "1101";
- bug gefixt, dass die Änderungen von der Oberfläche erst beim zweiten Mal übernommen wurden
- man kann jetzt auch 0 als MINIMUM_NACHFOLGER_ANZAHL einstellen, allerdings bekommt man oft
so nicht die gewünschte zu generierende Kontakt_Anzahl
- die Anzahl der Ebenen der Baumstruktur wurde am Ende nicht richtig ausgegeben. Es wurde immer
die Ebene vom zuletzt generierten Kontakt ausgegeben, nun wird die richtige Ebenenanzahl
ausgegeben
- ungerichtete formelle Beziehungen, gerichtete informelle Beziehungen
- Organisation über CEO



Es wird ab jetzt nur noch die englische Version des Testdatengenerator weiter verändert!!

Changes:

- Es werden jetzt zuerst die kontakte, dann die beziehungen geschrieben
- Organisationen sind vom Attribut Type her immer Organisation und haben noch zusäzlich das Attribut Role
- "null"-Attributwerte wurden durch leeren String "" ersetzt
- Es können keine Beziehungen mehr zum gleichem Knoten (Schleifen) erzeugt werden
- Prüfer durch "Pruefer" ersetzt
- Anlagen haben jetzt zusätzlich zu ihrem Link auch einen Namen(Name der Datei des Links)
- Die erzeugte Jsondatei ist jetzt UTF-8 codiert
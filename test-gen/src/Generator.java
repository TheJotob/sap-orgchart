import java.io.IOException;
import java.util.Vector;
import java.util.Random;
public class Generator
{
    public static int KONTAKT_ANZAHL = 100;
    public static int NACHFOLGER_MINIMUM = 3;
    public static int NACHFOLGER_MAXIMUM = 7;
    public static int BEZIEHUNGEN_INFORMELL_ANZAHL = KONTAKT_ANZAHL / 2;
    private Data data;
    public int aktEbene;
    public int maxEbene;
    private int aktID;
    private int aktKontaktID;
    private Kontakt [] kontakte;
    public int anzahlIndex;
    private Vector<Beziehung> beziehungen;
    private Vector<Kontakt> kontakteAkt;
    private Random random;
    public int anzahlAnsprechpartner;
    public int anzahlOrganisation;
    public Generator()
    {
    	data = new Data();
    	aktEbene = 0;
    	maxEbene = 0;
    	aktID = 1;
    	kontakte = new Kontakt [KONTAKT_ANZAHL];
    	anzahlIndex = 0;
    	anzahlAnsprechpartner = 0;
    	anzahlOrganisation = 0;
    	beziehungen = new Vector<Beziehung>(KONTAKT_ANZAHL);
    	kontakteAkt = new Vector<Kontakt>();
    	random = new Random();
    	random.setSeed(random.nextLong());
    }
    public void initialisieren()
    {
    	kontakte = new Kontakt[KONTAKT_ANZAHL];
    	beziehungen = new Vector<Beziehung>(KONTAKT_ANZAHL);
    	kontakteAkt = new Vector<Kontakt>();
    	aktEbene = 0;
    	maxEbene = 0;
    	aktID = 1;
    	anzahlIndex = 0;
    	anzahlAnsprechpartner = 0;
    	anzahlOrganisation = 0;
    }
    public static void setKontaktAnzahl(int neu)
    {
    	KONTAKT_ANZAHL = neu;
    }
    public static void setNachfolgerMinimum(int neu)
    {
    	NACHFOLGER_MINIMUM = neu;
    }
    public static void setNachfolgerMaximum(int neu)
    {
    	NACHFOLGER_MAXIMUM = neu;
    }
    
    public static void setBeziehungInformell(int neu)
    {
    	BEZIEHUNGEN_INFORMELL_ANZAHL = neu;
    }
    private void addBeziehung(Beziehung b)
    {
    	beziehungen.add(b);
    }
    private boolean containsBeziehung(Beziehung b)
    {
    	for(Beziehung bV : beziehungen)
    	{
    		if(bV.equals(b)) return true;
    	}
    	return false;
    }
    private void addKontakt(Kontakt k)
    {
    	kontakte[anzahlIndex++] = k;
    }
    
    public void generateAndWriteData()
    {
    	generateData();
    	JsondateiW.setKontaktAnzahl(anzahlIndex);
        JsondateiW.setBeziehungAnzahl(beziehungen.size());
        JsondateiW.initialisieren();
        Beziehung [] bez = new Beziehung[beziehungen.size()];
        beziehungen.copyInto(bez);
        
        try {
        JsondateiW.writeJsonDatei(bez, kontakte);
        } catch(IOException ex){ ex.printStackTrace(); }
    }
    private void generateData()
    {
    	Organisation o = generateOrganisation();
    	o.name1 = "MHP Sopro";
    	o.name2 = "A Porsche Company";
    	o.rolle = "Firma";
    	addKontakt(o);
    	++aktEbene;
    	Kontakt k = generateKontakt();
    	addKontakt(k);
    	String beziehung_typ = "10" + random.nextInt(2) + random.nextInt(2);
    	Beziehung b = new Beziehung(o.kontaktID,k.kontaktID,null,"Geschäftsführung","CEO",beziehung_typ);
    	b.adressen = generateAdressen();
		b.anlagen = generateAnlagen();
		b.emailadressen = generateEmailadressen(k);
		b.marketingattribute = generateMarketingattribute();
		b.notizen = generateNotizen();
		b.nummern = generateNummern();
    	addBeziehung(b);
    	//generateNachfolger(k,2);
    	generateKontakte();
    	kontakteAkt.add(k);
    	aktKontaktID = 1;
    	generateBeziehungen();
    	generateBeziehungenInformell();                        
    }
    private void generateBeziehungenInformell()
    {
    	int erst, zweit=0;
    	if(BEZIEHUNGEN_INFORMELL_ANZAHL > anzahlIndex)
    	{
    		BEZIEHUNGEN_INFORMELL_ANZAHL = anzahlIndex / 2;
    	}
    	for(int i = 0; i < 15 && i < BEZIEHUNGEN_INFORMELL_ANZAHL; ++i)
    	{
    		do{
			erst = random.nextInt(30)+1;
			if(random.nextDouble() > 0.75) zweit = random.nextInt(30)+1;
			if(KONTAKT_ANZAHL -1 <= erst) erst = random.nextInt(KONTAKT_ANZAHL);
			if(KONTAKT_ANZAHL -1 <= zweit) zweit = random.nextInt(KONTAKT_ANZAHL);
			
			
		    if(random.nextDouble() < 0.5) zweit = erst + 1;
	        else                          zweit = erst - 1;
    		}
    		while(erst == zweit || zweit == KONTAKT_ANZAHL || zweit == -1);
    		
    		String abteilung = data.getAbteilung(kontakte[zweit].ebene);
    		String beziehung_typ = "0" +random.nextInt(2) + random.nextInt(2) + random.nextInt(2);
    		Beziehung b = new Beziehung(kontakte[erst].kontaktID,kontakte[zweit].kontaktID,generateBeziehungInformellKomentar(),abteilung,data.getPosition(abteilung, kontakte[zweit].ebene),beziehung_typ);
    		if(containsBeziehung(b))
    		{
    			--i;
    			continue;
    		}
    		b.adressen = generateAdressen();
    		b.anlagen = generateAnlagen();
    		b.emailadressen = generateEmailadressen(kontakte[zweit]);
    		b.marketingattribute = generateMarketingattribute();
    		b.notizen = generateNotizen();
    		b.nummern = generateNummern();
    		addBeziehung(b);
    	}
    	for(int i = 15; i < BEZIEHUNGEN_INFORMELL_ANZAHL; ++i)
    	{
    		do{
    		erst = random.nextInt(anzahlIndex-1)+1;
    		zweit = random.nextInt(anzahlIndex-1)+1;
    		if(random.nextDouble() > 0.4)
    		{
    			if(random.nextDouble() < 0.05)
    			{
    				erst = random.nextInt(30)+1;
    				if(random.nextDouble() > 0.5) zweit = random.nextInt(30)+1;
    				if(KONTAKT_ANZAHL <= erst) erst = random.nextInt(KONTAKT_ANZAHL);
    			}
    			
    		    if(random.nextDouble() < 0.5) zweit = erst + 1;
    	        else                          zweit = erst - 1;  
    		}
    		}
    		while(erst == zweit || zweit == KONTAKT_ANZAHL || zweit == -1);
    		
    		String abteilung = data.getAbteilung(kontakte[zweit].ebene);
    		String beziehung_typ = "0" +random.nextInt(2) + random.nextInt(2) + random.nextInt(2);
    		Beziehung b = new Beziehung(kontakte[erst].kontaktID,kontakte[zweit].kontaktID,generateBeziehungInformellKomentar(),abteilung,data.getPosition(abteilung, kontakte[zweit].ebene),beziehung_typ);
    		if(containsBeziehung(b))
    		{
    			--i;
    			continue;
    		}
    		b.adressen = generateAdressen();
    		b.anlagen = generateAnlagen();
    		b.emailadressen = generateEmailadressen(kontakte[zweit]);
    		b.marketingattribute = generateMarketingattribute();
    		b.notizen = generateNotizen();
    		b.nummern = generateNummern();
    		addBeziehung(b);
    	}
    }
    private void generateKontakte()
    {
    	while(anzahlIndex < KONTAKT_ANZAHL) addKontakt(generateKontakt());
    }
    private void generateBeziehungen()
    {
    	++aktEbene;
    	int nachfolgerZahl;
    	Vector<Kontakt> erzeugteKinder = new Vector<Kontakt>();
    	for(Kontakt k: kontakteAkt)
    	{
    		nachfolgerZahl = random.nextInt(NACHFOLGER_MAXIMUM - NACHFOLGER_MINIMUM + 1) + NACHFOLGER_MINIMUM;
    		while(nachfolgerZahl > 0)
    		{
    			++ aktKontaktID;
    			if(aktKontaktID >= KONTAKT_ANZAHL) break;
    			kontakte[aktKontaktID].ebene = aktEbene;
    			Ansprechpartner a = null;
    			if(kontakte[aktKontaktID] instanceof Ansprechpartner)
    			{
    				 a = (Ansprechpartner) kontakte[aktKontaktID];
    			     a.ansprechpartner_Typ = data.getAnsprechpartnerTyp(aktEbene);
    			}
    			String abteilung = data.getAbteilung(aktEbene);
        		String beziehung_typ = "11" + random.nextInt(2) + random.nextInt(2);
        		Beziehung b = new Beziehung(k.kontaktID,kontakte[aktKontaktID].kontaktID,null,abteilung,data.getPosition(abteilung, aktEbene),beziehung_typ);
        		b.adressen = generateAdressen();
        		b.anlagen = generateAnlagen();
        		b.emailadressen = generateEmailadressen(kontakte[aktKontaktID]);
        		b.marketingattribute = generateMarketingattribute();
        		b.notizen = generateNotizen();
        		b.nummern = generateNummern();
        		addBeziehung(b);
        		erzeugteKinder.add(kontakte[aktKontaktID]);
        		-- nachfolgerZahl;
        		maxEbene = aktEbene;
    		}
    		if(aktKontaktID >= KONTAKT_ANZAHL) break;
    	}
    	if(aktKontaktID >= KONTAKT_ANZAHL) return;
    	kontakteAkt = erzeugteKinder;
    	generateBeziehungen();
    }
	
    private Kontakt generateKontakt()  // noch zu implementieren: mit geringer Wahrscheinlichkeit ist ein Kontakt auch eine Organisation statt Ansprechpartner
    {
    	if(random.nextDouble() > 0.07 || (aktID / 1.0 / KONTAKT_ANZAHL) > 0.1)  // Kontakt ist ein Ansprechpartner
    	{
    	   ++ anzahlAnsprechpartner;
    	   double zufall = random.nextDouble();
    	   String anrede; String vorname;
    	   if(zufall > 0.3)
    	   {
    		   anrede = "Herr";
    		   vorname = data.getVornameM();
    	   }
    	   else 
    	   {
    		   anrede = "Frau";
    		   vorname = data.getVornameW();
    	   }
    	   Ansprechpartner a = new Ansprechpartner(String.valueOf(aktID++),aktEbene,vorname,data.getNachname(),anrede,data.getTitel(),
    			                        data.getAnsprechpartnerTyp(aktEbene));
    	   a.anlagen = generateAnlagen();
    	   a.notizen = generateNotizen();
    	   a.marketingattribute = generateMarketingattribute();
    	   return a;
    	}
    	else  // Im seltenem Fall ist der Kontakt eine Organisation : erst ab der dritten Ebene möglich!
    	{
    		return generateOrganisation();
    	}
    }
    private Organisation generateOrganisation()
    {
    	++ anzahlOrganisation;
		String organisation = data.getOrganisation();
		int stelleD = organisation.indexOf("$");
		String rolle = organisation.substring(stelleD+1);
		organisation = organisation.substring(0,stelleD);
		int stelleP = organisation.indexOf("%");
		String name1, name2;
		if(stelleP != -1)
		{
			name1 = organisation.substring(0,stelleP);
			name2 = organisation.substring(stelleP+1);
		}
		else
		{
			name1 = organisation;
			name2 = null;
		}
		Organisation o = new Organisation(String.valueOf(aktID++),aktEbene,name1,name2,rolle);
		o.adressen = generateAdressen();
		o.anlagen = generateAnlagen();
		o.emailadressen = generateEmailadressen(o);
		o.marketingattribute = generateMarketingattribute();
		o.notizen = generateNotizen();
		o.nummern = generateNummern();
		return o;		
    }
    public Kontakt generateKontakt(int aktEbeneNeu)
    {
    	aktEbene = aktEbeneNeu;
    	return generateKontakt();
    }
    public Notiz[] generateNotizen()
    {
    	int z = random.nextInt(10);
    	int anzahlN = 0;
    	if(z < 4) return null;
    	else if(z < 6) anzahlN = 1;
    	else if(z < 8) anzahlN = 2;
    	else if(z == 8) anzahlN = 3;
    	else if(z == 9) anzahlN = 4;
    	Notiz[] n = new Notiz[anzahlN];
    	for(int i = 0; i < anzahlN; ++i)
    	{
    		n [i] = generateNotiz();
    	}
    	return n;
    }
    private Notiz generateNotiz()
    {
    	String sprache;
    	if(random.nextDouble() > 0.5) sprache = "deutsch";
    	else                          sprache = "englisch";
    	String text = data.getNotizText(sprache);
    	return new Notiz(text,sprache);
    }
    public Email[] generateEmailadressen(Kontakt k)
    {
    	int z = random.nextInt(10);
    	int anzahlE = 0;
    	if(z < 6) anzahlE = 1;
    	else if(z < 9) anzahlE = 2;
    	else if(z == 9) anzahlE = 3;
    	Email[] e = new Email[anzahlE];
    	for(int i = 0; i < anzahlE; ++i)
    	{
    		e [i] = generateEmail(k);
    	}
    	return e;
    }
    private Email generateEmail(Kontakt k)
    {
    	if(k instanceof Ansprechpartner)
    	{
    		Ansprechpartner a = (Ansprechpartner) k;
    		int z = random.nextInt(4);
    		String em = "";
    		if(z == 0) em = a.vorname+"."+a.nachname+"@gmx.de";
    		if(z == 1) em = a.vorname+"."+a.nachname+"@t-online.de";
    		if(z == 2) em = a.vorname+"."+a.nachname+"@aol.de";
    		if(z == 3) em = a.vorname+"."+a.nachname+"@yahoo.de";
    		return new Email(em);
    	}
    	if(k instanceof Organisation)
    	{
    		Organisation o = (Organisation) k;
    		int z = random.nextInt(4);
    		String em = "";
    		if(z == 0) em = o.name1+"."+"@recruiting.com";
    		if(z == 1) em = o.name1+"@support.com";
    		if(z == 2) em = o.name1+"@customerService.com";
    		if(z == 3) em = o.name1+"@supportService.com";
    		return new Email(em);
    	}
    	return null;
    }
    public Nummer[] generateNummern()
    {
    	int z = random.nextInt(10);
    	int anzahlN = 0;
    	if(z < 6) anzahlN = 1;
    	else if(z < 8) anzahlN = 2;
    	else if(z == 8) anzahlN = 3;
    	else if(z == 9) anzahlN = 4;
    	Nummer[] n = new Nummer[anzahlN];
    	for(int i = 0; i < anzahlN; ++i)
    	{
    		n [i] = generateNummer();
    	}
    	return n;
    }
    private Nummer generateNummer()
    {
    	String landesvorwahl = "";
    	int z = random.nextInt(4);
    	if(z == 0) landesvorwahl = "49";  // Deutschland
        if(z == 1) landesvorwahl = "39";  // Italien
        if(z == 2) landesvorwahl = "31";  // Niederlande
        if(z == 3) landesvorwahl = "44";  // Großbritannien
        String typ = "";
        int tz = random.nextInt(30);
        if(tz < 13) typ = "Telefon";
        else if(tz < 27) typ = "Handy";
        else typ = "Fax";
        String nummer = ""; String durchwahl = "";
        if(typ.equals("Telefon"))
        {
        	nummer = data.getNummer(0);
        	durchwahl = data.getDurchwahl();
        }
        if(typ.equals("Handy"))
        {
        	nummer = data.getNummer(1);
        	durchwahl = null;
        }
        if(typ.equals("Fax"))
        {
        	nummer = data.getNummer(2);
        	durchwahl = data.getDurchwahl();
        }
        return new Nummer(landesvorwahl,nummer,durchwahl,typ);
    }
    public Adresse[] generateAdressen()
    {
    	int z = random.nextInt(10);
    	int anzahlA = 0;
    	if(z < 6) anzahlA = 1;
    	else if(z < 8) anzahlA = 2;
    	else if(z == 8) anzahlA = 3;
    	else if(z == 9) anzahlA = 4;
    	Adresse[] a = new Adresse[anzahlA];
    	for(int i = 0; i < anzahlA; ++i)
    	{
    		a [i] = generateAdresse(String.valueOf(i+1));
    	}
    	return a;
    }
    private Adresse generateAdresse(String typ)
    {
    	String plz = data.getPostleitzahl();
    	String stadt = data.getOrt(plz);
    	String land = "Deutschland";
    	String strasse = data.getStrasse();
    	String hausnummer = data.getHausnummer();
    	return new Adresse(stadt,plz,strasse,hausnummer,land,typ);
    }
    public Marketingattribut[] generateMarketingattribute()
    {
    	int z = random.nextInt(10);
    	int anzahlM = 0;
    	if(z < 4) return null;
    	else if(z < 6) anzahlM = 1;
    	else if(z < 8) anzahlM = 2;
    	else if(z == 8) anzahlM = 3;
    	else if(z == 9) anzahlM = 4;
    	Marketingattribut[] m = new Marketingattribut[anzahlM];
    	for(int i = 0; i < anzahlM; ++i)
    	{
    		m [i] = generateMarketingattribut();
    	}
    	return m;
    }
    private Marketingattribut generateMarketingattribut()
    {
    	String attributgruppe = data.getMarketingattribut();
    	String name = "Zertifikat";
    	String wert = data.getDatum();
    	String beschreibung = "absolviert";
    	return new Marketingattribut(attributgruppe,name,wert,beschreibung);
    }
    public Anlage[] generateAnlagen()
    {
    	int z = random.nextInt(10);
    	int anzahlA = 0;
    	if(z < 5) return null;
    	else if(z < 7) anzahlA = 1;
    	else if(z < 8) anzahlA = 2;
    	else if(z == 8) anzahlA = 3;
    	else if(z == 9) anzahlA = 4;
    	Anlage[] a = new Anlage[anzahlA];
    	for(int i = 0; i < anzahlA; ++i)
    	{
    		a [i] = generateAnlage();
    	}
    	return a;
    }
    private Anlage generateAnlage()
    {
    	String link = data.getAnlageLink();
    	int stelle = link.lastIndexOf("/");
    	String typ = link.substring(stelle+1);
    	typ = typ.substring(0,typ.indexOf("."));
    	return new Anlage(typ,link);
    }
    private String generateBeziehungInformellKomentar()
    {
		return data.getBeziehungInformellKommentar();
	}
}

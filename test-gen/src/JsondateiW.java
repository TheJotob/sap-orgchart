import java.io.IOException;
import java.io.PrintWriter;
public class JsondateiW
{
    private static PrintWriter writer;
    public static int kontaktAnzahl;             // Anzahl der gesamten Kontakte
    public static int beziehungAnzahl;           // Anzahl der gesamten Beziehungen
    public static int aktKontaktIndex;
    public static int aktBeziehungIndex;
    public static String pfad = "Jsondateien/Contacts.json";
    public static void setKontaktAnzahl(int neu)
    {
    	kontaktAnzahl = neu;
    }
    public static void setBeziehungAnzahl(int neu)
    {
    	beziehungAnzahl = neu;
    }
    public static void initialisieren()
    {
    	aktKontaktIndex = 0;
    	aktBeziehungIndex = 0;
    }
    public static int getContactWrittenCount()
    {
    	return aktKontaktIndex;
    }
    public static int getRelationshipWrittenCount()
    {
    	return aktBeziehungIndex;
    }
    public static void writeJsonDatei(Beziehung [] beziehungen, Kontakt[] kontakte) throws IOException
    {
    	writer = new PrintWriter(pfad,"UTF-8");
    	writer.print("{");
    	writer.print(konvertString("kontakte") + ":[");
    	for(int i = 0; i < kontaktAnzahl; ++i)
    	{
    		if(i > 0) writer.print(",");
    		if(kontakte[i] != null) writeKontakt(kontakte[i]);
    	}
    	writer.print("],");
    	
    	writer.print(konvertString("beziehungen") + ":[");
    	for(int i = 0; i < beziehungAnzahl; ++i)
    	{
    		if(i > 0) writer.print(",");
    		if(beziehungen[i] != null) writeBeziehung(beziehungen[i]);
    	}
    	writer.print("]");
    	
    	writer.print("}");
    	writer.close();
    }
    public static void writeKontakt(Kontakt k) throws IOException
    {
    	writer.print("{");
		writer.print(konvertString("Contact_ID") + ":" + konvertString(k.kontaktID)+",");
		writer.print(konvertString("Level") + ":" + k.ebene+",");
		
    	if(k instanceof Ansprechpartner)
    	{
    		Ansprechpartner a = (Ansprechpartner) k;
    		writer.print(konvertString("Firstname") + ":" + konvertString(a.vorname)+",");
    		writer.print(konvertString("Lastname") + ":" + konvertString(a.nachname)+",");
    		writer.print(konvertString("Title_Key") + ":" + konvertString(a.anrede)+",");
    		writer.print(konvertString("Academic_Title") + ":" + konvertString(a.titel)+",");
    		writer.print(konvertString("Type") + ":" + konvertString(a.ansprechpartner_Typ)+",");
    		writer.print(konvertString("notiz") + ":[");      // Notizen
        	if(a.notizen == null) writer.print("],");    
        	else
        	{
        		for(int i = 0; i < a.notizen.length && a.notizen[i] != null; ++i)
        		{
        		  writer.print("{");
        		  writer.print(konvertString("Notiz") + ":" + konvertString(a.notizen[i].text)+",");
        		  writer.print(konvertString("Notiztyp") + ":" + konvertString(a.notizen[i].sprache));
        		  if( (i+1) < a.notizen.length && a.notizen[i+1] != null)
        		  writer.print("},");
        		  else
        		  writer.print("}");
        		}
        		writer.print("],");
        	}
        	writer.print(konvertString("marketingattribut") + ":[");  // Marketingattribute
        	if(a.marketingattribute == null) writer.print("],");
        	else
        	{
        		for(int i = 0; i < a.marketingattribute.length && a.marketingattribute[i] != null; ++i)
        		{
        		  writer.print("{");
        		  writer.print(konvertString("Attribute Set") + ":" + konvertString(a.marketingattribute[i].attributgruppe)+",");
        		  writer.print(konvertString("Attribute Name") + ":" + konvertString(a.marketingattribute[i].name)+",");
        		  writer.print(konvertString("Attribute Value") + ":" + konvertString(a.marketingattribute[i].wert)+",");
        		  writer.print(konvertString("Attribute Description") + ":" + konvertString(a.marketingattribute[i].beschreibung));
        		  if( (i+1) < a.marketingattribute.length && a.marketingattribute[i+1] != null)
        		  writer.print("},");
        		  else
        		  writer.print("}");
        		}
        		writer.print("],");
        	}
        	writer.print(konvertString("anlage") + ":[");  // Anlagen
        	if(a.anlagen == null) writer.print("]");
        	else
        	{
        		for(int i = 0; i < a.anlagen.length && a.anlagen[i] != null; ++i)
        		{
        		  writer.print("{");
        		  writer.print(konvertString("Attachement-Name") + ":" + konvertString(a.anlagen[i].name)+",");
        		  writer.print(konvertString("Attachement-URL") + ":" + konvertString(a.anlagen[i].typ));
        		  if( (i+1) < a.anlagen.length && a.anlagen[i+1] != null)
        		  writer.print("},");
        		  else
        		  writer.print("}");
        		}
        		writer.print("]");
        	}
        	
    	}
    	if(k instanceof Organisation)
    	{
    		Organisation o = (Organisation) k;
    		writer.print(konvertString("Name1") + ":" + konvertString(o.name1)+",");
    		writer.print(konvertString("Name2") + ":" + konvertString(o.name2)+",");
    		writer.print(konvertString("Role") + ":" + konvertString(o.rolle)+",");
    		writer.print(konvertString("Type") + ":" + konvertString("Organisation")+",");
    		
    		writer.print(konvertString("notiz") + ":[");      // Notizen
        	if(o.notizen == null) writer.print("],");    
        	else
        	{
        		for(int i = 0; i < o.notizen.length && o.notizen[i] != null; ++i)
        		{
        		  writer.print("{");
        		  writer.print(konvertString("Notiz") + ":" + konvertString(o.notizen[i].text)+",");
        		  writer.print(konvertString("Notiztyp") + ":" + konvertString(o.notizen[i].sprache));
        		  if( (i+1) < o.notizen.length && o.notizen[i+1] != null)
        		  writer.print("},");
        		  else
        		  writer.print("}");
        		}
        		writer.print("],");
        	}
        	writer.print(konvertString("e-Mail") + ":[");  // E-Mail_Adressen
        	if(o.emailadressen == null) writer.print("],");
        	else
        	{
        		for(int i = 0; i < o.emailadressen.length && o.emailadressen[i] != null; ++i)
        		{
        		  writer.print("{");
        		  writer.print(konvertString("E-Mail") + ":" + konvertString(o.emailadressen[i].email_Adresse));
        		  if( (i+1) < o.emailadressen.length && o.emailadressen[i+1] != null)
        		  writer.print("},");
        		  else
        		  writer.print("}");
        		}
        		writer.print("],");
        	}
        	writer.print(konvertString("nummer") + ":[");  // Nummern
        	if(o.nummern == null) writer.print("],");
        	else
        	{
        		for(int i = 0; i < o.nummern.length && o.nummern[i] != null; ++i)
        		{
        		  writer.print("{");
        		  writer.print(konvertString("Countryiso") + ":" + konvertString(o.nummern[i].landesvorwahl)+",");
        		  writer.print(konvertString("Number") + ":" + konvertString(o.nummern[i].nummer)+",");
        		  writer.print(konvertString("Extension") + ":" + konvertString(o.nummern[i].durchwahl)+",");
        		  writer.print(konvertString("Number_Type") + ":" + konvertString(o.nummern[i].nummer_Typ));
        		  if( (i+1) < o.nummern.length && o.nummern[i+1] != null)
        		  writer.print("},");
        		  else
        		  writer.print("}");
        		}
        		writer.print("],");
        	}
        	writer.print(konvertString("adresse") + ":[");  // Adressen
        	if(o.adressen == null) writer.print("],");
        	else
        	{
        		for(int i = 0; i < o.adressen.length && o.adressen[i] != null; ++i)
        		{
        		  writer.print("{");
        		  writer.print(konvertString("City") + ":" + konvertString(o.adressen[i].stadt)+",");
        		  writer.print(konvertString("Post Code") + ":" + konvertString(o.adressen[i].postleitzahl)+",");
        		  writer.print(konvertString("Street") + ":" + konvertString(o.adressen[i].strasse)+",");
        		  writer.print(konvertString("House No.") + ":" + konvertString(o.adressen[i].hausnummer)+",");
        		  writer.print(konvertString("Country") + ":" + konvertString(o.adressen[i].land)+",");
        		  writer.print(konvertString("Address_Type") + ":" + konvertString(o.adressen[i].adresse_Typ));
        		  if( (i+1) < o.adressen.length && o.adressen[i+1] != null)
        		  writer.print("},");
        		  else
        		  writer.print("}");
        		}
        		writer.print("],");
        	}
        	writer.print(konvertString("marketingattribut") + ":[");  // Marketingattribute
        	if(o.marketingattribute == null) writer.print("],");
        	else
        	{
        		for(int i = 0; i < o.marketingattribute.length && o.marketingattribute[i] != null; ++i)
        		{
        		  writer.print("{");
        		  writer.print(konvertString("Attribute Set") + ":" + konvertString(o.marketingattribute[i].attributgruppe)+",");
        		  writer.print(konvertString("Attribute Name") + ":" + konvertString(o.marketingattribute[i].name)+",");
        		  writer.print(konvertString("Attribute Value") + ":" + konvertString(o.marketingattribute[i].wert)+",");
        		  writer.print(konvertString("Attribute Description") + ":" + konvertString(o.marketingattribute[i].beschreibung));
        		  if( (i+1) < o.marketingattribute.length && o.marketingattribute[i+1] != null)
        		  writer.print("},");
        		  else
        		  writer.print("}");
        		}
        		writer.print("],");
        	}
        	writer.print(konvertString("anlage") + ":[");  // Anlagen
        	if(o.anlagen == null) writer.print("]");
        	else
        	{
        		for(int i = 0; i < o.anlagen.length && o.anlagen[i] != null; ++i)
        		{
        		  writer.print("{");
        		  writer.print(konvertString("Attachement-Name") + ":" + konvertString(o.anlagen[i].name)+",");
        		  writer.print(konvertString("Attachement-URL") + ":" + konvertString(o.anlagen[i].typ));
        		  if( (i+1) < o.anlagen.length && o.anlagen[i+1] != null)
        		  writer.print("},");
        		  else
        		  writer.print("}");
        		}
        		writer.print("]");
        	}
    	}
    	
    	
    	writer.print("}");
    	++aktKontaktIndex;
    }
    public static void writeBeziehung(Beziehung b) throws IOException
    {
    	writer.print("{");
    	writer.print("\"Partner1_ID\":"   +konvertString(b.kontakt_ID1)+",");
    	writer.print("\"Partner2_ID\":"   +konvertString(b.kontakt_ID2)+",");
    	writer.print("\"Comment\":"     +konvertString(b.kommentar)+",");
    	writer.print("\"Department_Name\":"+konvertString(b.abteilung_Name)+",");
    	writer.print("\"Function_Name\":" +konvertString(b.funktion_Name)+",");
    	writer.print("\"Relationship_Type\":" +konvertString(b.beziehung_Typ)+",");
    	
    	writer.print(konvertString("notiz") + ":[");      // Notizen
    	if(b.notizen == null) writer.print("],");    
    	else
    	{
    		for(int i = 0; i < b.notizen.length && b.notizen[i] != null; ++i)
    		{
    		  writer.print("{");
    		  writer.print(konvertString("Notiz") + ":" + konvertString(b.notizen[i].text)+",");
    		  writer.print(konvertString("Notiztyp") + ":" + konvertString(b.notizen[i].sprache));
    		  if( (i+1) < b.notizen.length && b.notizen[i+1] != null)
    		  writer.print("},");
    		  else
    		  writer.print("}");
    		}
    		writer.print("],");
    	}
    	writer.print(konvertString("e-Mail") + ":[");  // E-Mail_Adressen
    	if(b.emailadressen == null) writer.print("],");
    	else
    	{
    		for(int i = 0; i < b.emailadressen.length && b.emailadressen[i] != null; ++i)
    		{
    		  writer.print("{");
    		  writer.print(konvertString("E-Mail") + ":" + konvertString(b.emailadressen[i].email_Adresse));
    		  if( (i+1) < b.emailadressen.length && b.emailadressen[i+1] != null)
    		  writer.print("},");
    		  else
    		  writer.print("}");
    		}
    		writer.print("],");
    	}
    	writer.print(konvertString("nummer") + ":[");  // Nummern
    	if(b.nummern == null) writer.print("],");
    	else
    	{
    		for(int i = 0; i < b.nummern.length && b.nummern[i] != null; ++i)
    		{
    		  writer.print("{");
    		  writer.print(konvertString("Countryiso") + ":" + konvertString(b.nummern[i].landesvorwahl)+",");
    		  writer.print(konvertString("Number") + ":" + konvertString(b.nummern[i].nummer)+",");
    		  writer.print(konvertString("Extension") + ":" + konvertString(b.nummern[i].durchwahl)+",");
    		  writer.print(konvertString("Number_Type") + ":" + konvertString(b.nummern[i].nummer_Typ));
    		  if( (i+1) < b.nummern.length && b.nummern[i+1] != null)
    		  writer.print("},");
    		  else
    		  writer.print("}");
    		}
    		writer.print("],");
    	}
    	writer.print(konvertString("adresse") + ":[");  // Adressen
    	if(b.adressen == null) writer.print("],");
    	else
    	{
    		for(int i = 0; i < b.adressen.length && b.adressen[i] != null; ++i)
    		{
    		  writer.print("{");
    		  writer.print(konvertString("City") + ":" + konvertString(b.adressen[i].stadt)+",");
    		  writer.print(konvertString("Post Code") + ":" + konvertString(b.adressen[i].postleitzahl)+",");
    		  writer.print(konvertString("Street") + ":" + konvertString(b.adressen[i].strasse)+",");
    		  writer.print(konvertString("House No.") + ":" + konvertString(b.adressen[i].hausnummer)+",");
    		  writer.print(konvertString("Country") + ":" + konvertString(b.adressen[i].land)+",");
    		  writer.print(konvertString("Address_Type") + ":" + konvertString(b.adressen[i].adresse_Typ));
    		  if( (i+1) < b.adressen.length && b.adressen[i+1] != null)
    		  writer.print("},");
    		  else
    		  writer.print("}");
    		}
    		writer.print("],");
    	}
    	writer.print(konvertString("marketingattribut") + ":[");  // Marketingattribute
    	if(b.marketingattribute == null) writer.print("],");
    	else
    	{
    		for(int i = 0; i < b.marketingattribute.length && b.marketingattribute[i] != null; ++i)
    		{
    		  writer.print("{");
    		  writer.print(konvertString("Attribute Set") + ":" + konvertString(b.marketingattribute[i].attributgruppe)+",");
    		  writer.print(konvertString("Attribute Name") + ":" + konvertString(b.marketingattribute[i].name)+",");
    		  writer.print(konvertString("Attribute Value") + ":" + konvertString(b.marketingattribute[i].wert)+",");
    		  writer.print(konvertString("Attribute Description") + ":" + konvertString(b.marketingattribute[i].beschreibung));
    		  if( (i+1) < b.marketingattribute.length && b.marketingattribute[i+1] != null)
    		  writer.print("},");
    		  else
    		  writer.print("}");
    		}
    		writer.print("],");
    	}
    	writer.print(konvertString("anlage") + ":[");  // Anlagen
    	if(b.anlagen == null) writer.print("]");
    	else
    	{
    		for(int i = 0; i < b.anlagen.length && b.anlagen[i] != null; ++i)
    		{
    		  writer.print("{");
    		  writer.print(konvertString("Attachement-Name") + ":" + konvertString(b.anlagen[i].name)+",");
    		  writer.print(konvertString("Attachement-URL") + ":" + konvertString(b.anlagen[i].typ));
    		  if( (i+1) < b.anlagen.length && b.anlagen[i+1] != null)
    		  writer.print("},");
    		  else
    		  writer.print("}");
    		}
    		writer.print("]");
    	}
    	
    	writer.print("}");
    	++aktBeziehungIndex;
    }
    private static String konvertString(String s)
    {
    	if(s != null)
    	{
    		s = "\"" + s + "\"";
    	}
    	if(s == null)   // statt null der leere String ""
    	{
    		s = "\"\"";
    	}
    	return s;
    }
}

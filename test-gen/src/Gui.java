import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.GridLayout;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Dimension;

public class Gui extends JFrame implements ActionListener
{
	private static final long serialVersionUID = -8227852865493985051L;
	Generator generator;
    private JTextField text1, text2, text3, text4;
    private JLabel     label1, label2, label3, label4;
    private JButton generieren;
    public Gui()
    {
    	super("Testdatengenerator (Final)");
    	generator = new Generator();
    	JPanel panelOben = new JPanel();
        panelOben.setLayout(new GridLayout(4,2,5,5));
        label1 = new JLabel("Kontaktanzahl:");
        label2 = new JLabel("Miminum_Nachfolger_Zahl:");
        label3 = new JLabel("Maximum_Nachfolger_Zahl:");
        label4 = new JLabel("Informelle Beziehunganzahl:");
        text1 = new JTextField("100");
        text2 = new JTextField("3");
        text3 = new JTextField("7");
        text4 = new JTextField("50");
        text1.setFont(new Font("Verdana",Font.PLAIN,20));
        text2.setFont(new Font("Verdana",Font.PLAIN,20));
        text3.setFont(new Font("Verdana",Font.PLAIN,20));
        text4.setFont(new Font("Verdana",Font.PLAIN,20));
        generieren = new JButton("Testdaten generieren");
        generieren.setFont(new Font("Verdana",Font.PLAIN,20));
        generieren.addActionListener(this);
        generieren.setPreferredSize(new Dimension(400,80));
    	panelOben.add(label1);
    	panelOben.add(text1);
    	panelOben.add(label2);
    	panelOben.add(text2);
    	panelOben.add(label3);
    	panelOben.add(text3);
    	panelOben.add(label4);
    	panelOben.add(text4);
    	add(panelOben,BorderLayout.CENTER);
    	add(generieren,BorderLayout.SOUTH);
        setSize(400,500);	
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
   	    setVisible(true);

    }
    
	public void actionPerformed(ActionEvent e)
	{
		if(e.getSource() == generieren)
		{
			String kontakte = text1.getText();
			String min = text2.getText();
			String max = text3.getText();
			String inf = text4.getText();
			int kZ = -1, minZ = -1, maxZ = -1, infZ = -1;
			try
			{
				kZ = Integer.parseInt(kontakte);
				minZ = Integer.parseInt(min);
				maxZ = Integer.parseInt(max);
				infZ = Integer.parseInt(inf);
			} 
			catch(NumberFormatException ex){};
			
			if(kZ > 9)    Generator.setKontaktAnzahl(kZ);
			if(minZ > -1)  Generator.setNachfolgerMinimum(minZ);
			if(maxZ > -1)  Generator.setNachfolgerMaximum(maxZ);
			if(infZ > -1) Generator.setBeziehungInformell(infZ);
			
			generator.initialisieren();
			generator.generateAndWriteData();
			String meldung;
			
			if(Generator.KONTAKT_ANZAHL == generator.anzahlIndex)
			{
			meldung = "Es wurden insgesamt " + generator.anzahlIndex+ " Kontakte (" + generator.anzahlAnsprechpartner + " Ansprechpartner, "
			+ generator.anzahlOrganisation + " Organisation(en) ) erzeugt.\n"
			+ "Die generierte Baumstruktur besitzt "+ generator.maxEbene+ " Ebenen.\n";
			  if(JsondateiW.aktKontaktIndex == 0)
			  {
				meldung += "Es konnte keine Jsondatei geschrieben werden! Bitte erstellen Sie einen Ordner \"Jsondateien\" im gleichem Verzeichnis wie der Testdatengenerator!";
			  }
			  else
			  {
			    meldung += "Anzahl der geschriebenen Kontakte : "+ JsondateiW.aktKontaktIndex+"\n";
			    meldung += "Anzahl der geschriebenen Beziehungen : "+ JsondateiW.aktBeziehungIndex;
			  }
			}
			
			else
			{
			meldung = "Es wurde nicht die gew�nschte Kontaktanzahl generiert!!! Es wurden nur "+generator.anzahlIndex+ " von gew�nschten "+ Generator.KONTAKT_ANZAHL+" Kontakten generiert\n"
			+ "Die generierte Baumstruktur besitzt "+ generator.maxEbene+ " Ebenen.\n";
			meldung += "Versuchen Sie es erneut und �ndern sie gegebenenfalls die Wahl der Parameter!!\n";
			meldung += "Anzahl der geschriebenen Kontakte : "+ JsondateiW.aktKontaktIndex+"\n";
		    meldung += "Anzahl der geschriebenen Beziehungen : "+ JsondateiW.aktBeziehungIndex;
			}
			JOptionPane.showMessageDialog(this, meldung);
		}
	}
	public static void main(String[] args)
	{
		new Gui();
	}

}

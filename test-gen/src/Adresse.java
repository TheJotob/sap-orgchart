
public class Adresse
{
    String stadt;
    String postleitzahl;
    String strasse;
    String hausnummer;
    String land;
    String adresse_Typ;
    public Adresse(String stadt, String postleitzahl, String strasse, String hausnummer, String land, String typ)
    {
    	this.stadt = stadt;
    	this.postleitzahl = postleitzahl;
    	this.strasse = strasse;
    	this.hausnummer = hausnummer;
    	this.land = land;
    	adresse_Typ = typ;
    }
}


public class Ansprechpartner extends Kontakt
{
   String vorname;
   String nachname;
   String anrede;
   String titel;
   String ansprechpartner_Typ;
   Anlage [] anlagen;
   Notiz [] notizen;
   Marketingattribut [] marketingattribute;
   public Ansprechpartner(String id, int ebene, String vorname, String nachname, String anrede, String titel, String typ)
   {
	   super(id,ebene);
	   this.vorname = vorname;
	   this.nachname = nachname;
	   this.anrede = anrede;
	   this.titel = titel;
	   ansprechpartner_Typ = typ;
   }
}

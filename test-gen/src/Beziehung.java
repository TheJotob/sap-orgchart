
public class Beziehung
{
    String kontakt_ID1;
    String kontakt_ID2;
    String kommentar;
    String abteilung_Name;
    String funktion_Name;
    String beziehung_Typ;
    Notiz [] notizen;
    Email [] emailadressen;
    Nummer [] nummern;
    Adresse [] adressen;
    Marketingattribut [] marketingattribute;
    Anlage [] anlagen;
    public Beziehung(String k1, String k2, String kommentar, String abteilung_Name, String funktion_Name, String typ)
    {
    	kontakt_ID1 = k1;
    	kontakt_ID2 = k2;
    	this.kommentar = kommentar;
    	this.abteilung_Name = abteilung_Name;
    	this.funktion_Name = funktion_Name;
    	beziehung_Typ = typ;
    }
    public boolean equals(Beziehung b)
    {
    	if(kontakt_ID1.equals(b.kontakt_ID1) && kontakt_ID2.equals(b.kontakt_ID2) && beziehungTypV(b)) return true;
    	return false;
    }
    private boolean beziehungTypV(Beziehung b)
    {
    	String bez = beziehung_Typ.substring(0,2);
    	String bez2 = b.beziehung_Typ.substring(0,2);
    	return bez.equals(bez2);
    }
}

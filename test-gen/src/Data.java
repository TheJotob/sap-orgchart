import java.util.Random;
public class Data
{
   private Random random;
   private String [] vornamenW;   // 100 St�ck
   private String [] vornamenM;   // 100 St�ck
   private String [] nachnamen;   // 100 St�ck
   private String [] titel;       // 17 ( 10 mal null)
   private String [] abteilungen; // 19
   private String [] positionen;  // 24
   public Data()
   {
	   random = new Random();
	   random.setSeed(random.nextLong());
	   werteZuweisen();
   }
   private void werteZuweisen()
   {
	   vornamenW = new String []
			   {"Emma", "Mia", "Marie", "Sophie", "Astrid", "Anna", "Leonie", "Thea", "Emilia", "Lena", "Sophia", "Amelie", "Lina", "Lea", "Laura", "Lara", "Luisa", "Emily", "Nele", "Mila", 
			   "Charlotte", "Esme", "Johanna", "Lisa", "Fabienne", "Isa", "Hanna", "Hannah", "Lilly", "Mieke", "Lilith", "Maja", "Sarah", "Julia", "Leni", "Pia", "Alina", "Katharina", 
			   "Magdalena", "Lia", "Paula", "Mathilda", "Greta", "Lotta", "Maria", "Luna", "Isabella", "Melina", "Jana", "Helena", "Zoe", "Frieda", "Nora", "Ella", "Clara", "Sofia", 
			   "Klara", "Amalia", "Victoria", "Matilda", "Finja", "Elisa", "Annika", "Maya", "Stella", "Mara", "Jasmin", "Louisa", "Franziska", "Marlene", "Merle", "Antonia", "Mira", 
			   "Nina", "Luise", "Theresa", "Pauline", "Elena", "Valentina", "Lilli", "Eva", "Romy", "Frida", "Linda", "Josephine", "Lucy", "Fiona", "Lotte", "Sara", "Elisabeth",
			   "Milena", "Helene", "Selina", "Anne", "Rosalie", "Emilie", "Felicitas", "Emmi", "Kathrin", "Jennifer", "Yasmin"};
	   
	   vornamenM = new String []
			   {"Leon", "Ben", "Tom", "Paul", "Jonas", "Luca", "Lars", "Finn", "Patrick", "Nico", "Elias", "Neo", "Maximilian", "Max", "Lukas", "Noah", "Felix", "Julian", "Emil", "David", 
			   "Liam", "Moritz", "Alexander", "Jakob", "Tim", "Linus", "Luis", "Louis", "Anton", "Mats", "Leo", "Theo", "Mika", "Milan", "Jonathan", "Niklas", "Jan", "Henry", "Levi", 
			   "Samuel", "Lennard", "Phillip", "Sebastian", "Oskar", "Vincent", "Johannes", "Julius", "Fabian", "Benjamin", "Simon", "Daniel", "Gabriel", "Hannes", "Mateo", "Kilian", 
			   "Florian", "Adrian", "Marlon", "Noel", "Bastian", "Michael", "Tobias", "Valentin", "Jonah", "Rafael", "Jona", "Till", "Karl", "Marvin", "Justus", "Maxim", "Joshua", 
			   "Finn", "Malte", "Aaron", "Erik", "Thomas", "Nils", "Burkhard", "Leonard", "Konstantin", "Benedikt", "Nick", "Robin", "Christian", "Ole", "Joel", "Leander", "Levin", 
			   "Andreas", "Manuel", "Mattis", "Pepe", "Lucas", "Milo", "Timo", "Sam","Bennet", "Kevin", "Oliver"};
	   
	   nachnamen = new String []
			   {"M�ller", "Schmidt", "Schneider", "Fischer", "Weber", "Meyer", "Wagner", "Becker", "Schulz", "Hoffmann",
			   "Sch�fer", "Koch", "Bauer", "Richter", "Klein", "Wolf", "Schr�der", "Neumann", "Schwarz", "Zimmermann",
			   "Braun", "Kr�ger", "Hoffmann", "Hartmann", "Lange", "Schmitt", "Werner", "Schmitz", "Krause", "Meier",
			   "Lehmann", "Schmid", "Schulze", "Maier", "K�hler", "Herrmann", "K�nig", "Walter", "Mayer", "Huber",
			   "Kaiser", "Fuchs", "Peters", "Lang", "Scholz", "M�ller", "Wei�", "Jung", "Hahn", "Schubert",
			   "Vogel", "Friedrich", "Keller", "G�nter", "Frank", "Berger", "Winkler", "Roth", "Beck", "Lorenz",
			   "Baumann", "Franke", "Albrecht", "Schuster", "Simon", "Ludwig", "B�hm", "Winter", "Kraus", "Martin",
			   "Schuhmacher", "Kr�mer", "Vogt", "Stein", "J�ger", "Otto", "Sommer", "Gro�", "Seidel", "Heinrich",
			   "Brandt", "Haas", "Schreiber", "Graf", "Schulte", "Diedrich", "Ziegler", "Kuhn", "K�hn", "Pohl",
			   "Engel", "Horn", "Busch", "Bergmann", "Thomas", "Voigt", "Sauer", "Arnold", "Wolff", "Pfeiffer"};
	   
	   titel = new String []
			   {null, null, null, null, null, null, null, null, null, null,
			   "Prof.", "Dr.", "Dr.", "Dr.", "Dr. Ing.", "Dr. Ing.", "Dipl. Ing."};
	   
	   abteilungen = new String []
			   {"Gesch�ftsf�hrung", "Personalabteilung", "Buchhaltung", "Einkauf", "Finanzen", "Rechtsabteilung", "Forschung & Entwicklung", "Produktion", "Qualit�tssicherung",
			   "Arbeitssicherheit", "�ffentlichkeitsarbeit", "Marketing", "IT", "Hardwareadministration", "Vertrieb", "Logistik", "Poststelle", "Kundenbetreuung",
			   "Hausverwaltung"};
	   
	   positionen = new String []
			   {"CEO", "Vorstand", "Aufsichtsratmitglied", "Analyst", "Eink�ufer", "Einkaufsleiter", "Finanzleiter", "Generaldirektor", "Gesch�ftsf�hrer", "Gesellschafter",
			   "Handelsvertreter", "Angestellter", "Kundendienstmitarbeiter", "Lagerleiter", "Lagerarbeiter", "leitender Angestellter", "Leiter Finanzabteilung", 
			   "Sachbearbeiter", "Techniker", "Verk�ufer","Verkaufsleiter", "Vertriebsleiter", "Verwaltungsleiter", "Volkswirt"};
			   
			   
			   
			   
   }
   public String getVornameW()
   {
	   return vornamenW[random.nextInt(vornamenW.length)];
   }
   public String getVornameM()
   {
	   return vornamenM[random.nextInt(vornamenM.length)];
   }
   public String getNachname()
   {
	   return nachnamen[random.nextInt(nachnamen.length)];
   }
   public String getTitel()
   {
	   return titel[random.nextInt(titel.length)];
   }
   public String getAbteilung(int ebene)
   {
	   if(ebene == 1 || ebene == 2) return abteilungen[0];
	   else return abteilungen[random.nextInt(abteilungen.length-1)+1];
   }
   public String getPosition(String abteilung, int ebene)
   {
	   if(ebene == 1) return positionen[0];
	   int r;
	   if(ebene == 2)
	   {
		   r = random.nextInt(4);
		   if(r == 0) return positionen[1];
		   if(r == 1) return positionen[2];
		   if(r == 2) return positionen[7];
		   if(r == 3) return positionen[8];
	   }
	   else
	   {
		   r = random.nextInt(positionen.length);
		   if(r == 0 || r == 1 || r ==2 || r == 7 || r == 8) return getPosition(abteilung,ebene);
	   }
	   return positionen[r];
   }
   public String getAnsprechpartnerTyp(int ebene)
   {
	   if(ebene == 1) return "Genehmiger";
	   if(ebene == 2) return "Entscheider";
	   if(ebene == 3 || ebene == 4) 
	   {
		   if(random.nextDouble() > 0.125) return "Pruefer";
		   else                           return "Coach";
	   }
	   else
	   {
		   if(random.nextDouble() > 0.2)  return "Anwender";
		   else                           return "Coach";
	   }
   }
   public String getNotizText(String sprache)
   {
	   int r = random.nextInt(10);
	   if(sprache.equals("deutsch"))
	   {
		   switch(r)
		   {
		        case 0: return "arbeitet gut";
		        case 1: return "arbeitet fleisig";
		        case 2: return "verf�gt �ber gute soziale Kompetenzen";
		        case 3: return "versp�tet sich oft";
		        case 4: return "arbeitet sehr motiviert";
		        case 5: return "arbeitet wenig motiviert";
		        case 6: return "ist immer sehr p�nktlich";
		        case 7: return "ist sehr ehrgeizig";
		        case 8: return "ist sehr zielstrebig";
		        default: return "ist wenig zielstrebig";
		   }
	   }
	   if(sprache.equals("englisch"))
	   {
		   switch(r)
		   {
		        case 0: return "works good";
		        case 1: return "works with excellent conzentration";
		        case 2: return "good social skills";
		        case 3: return "comes often too late";
		        case 4: return "works with great motivation";
		        case 5: return "works without motivation";
		        case 6: return "very ambitios";
		        case 7: return "not ambitios";
		        case 8: return "great communication skills";
		        default: return "bad communication skills";
		   }
	   }
	   else throw new IllegalArgumentException("Sprache ung�ltig!!");
   }
   public String getNummer(int typ)
   {
	   int z = random.nextInt(10);
	   int nummer5 = random.nextInt(99999);
	   while(String.valueOf(nummer5).length() < 5) nummer5 = random.nextInt(99999);
	   int nummer6 = random.nextInt(999999);
	   while(String.valueOf(nummer6).length() < 6) nummer6 = random.nextInt(999999);
	   int nummer7 = random.nextInt(9999999);
	   while(String.valueOf(nummer7).length() < 7) nummer7 = random.nextInt(9999999);
	   int nummer8 = random.nextInt(99999999);
	   while(String.valueOf(nummer8).length() < 8) nummer8 = random.nextInt(99999999);
	   int nummer9 = random.nextInt(999999999);
	   while(String.valueOf(nummer9).length() < 9) nummer9 = random.nextInt(999999999);
	   int nummer10 = random.nextInt(1999999999);
	   while(String.valueOf(nummer10).length() < 10) nummer10 = random.nextInt(1999999999);
	   long nummerL1 = random.nextLong();
	   while(String.valueOf(nummerL1).length() < 8) nummerL1 = random.nextLong();
	   long nummerL2 = random.nextLong();
	   while(String.valueOf(nummerL2).length() < 9) nummerL2 = random.nextLong();
	   if(typ == 0 || typ == 2)
	   {
		   switch(z)
		   {
		       case 0: return "8231"+nummer5;
		       case 1: return "8234"+nummer6;
		       case 2: return "821"+nummer7;
		       case 3: return "89"+nummer8;
		       case 4: return "30"+nummer9;
		       case 5: return "221"+nummer10;
		       case 6: return "40"+nummerL1;
		       case 7: return "69"+nummerL2;
		       case 8: return "911"+nummer7;
		       default: return "6093"+nummer6;
		   }
	   }
	   if(typ == 1)
	   {
		   switch(z)
		   {
		       case 0: return "176"+nummer5;
		       case 1: return "179"+nummer6;
		       case 2: return "162"+nummer7;
		       case 3: return "172"+nummer8;
		       case 4: return "173"+nummer9;
		       case 5: return "174"+nummer10;
		       case 6: return "160"+nummerL1;
		       case 7: return "170"+nummerL2;
		       case 8: return "171"+nummer7;
		       default: return "175"+nummer8;
		   }
	   }
	   throw new IllegalArgumentException("Illegaler Typ f�r Nummer !!");
   }
   
   public String getDurchwahl()
   {
	   int z = random.nextInt(10);
	   int zahl3 = random.nextInt(999);
	   int zahl2 = random.nextInt(99);
	   int zahl4 = random.nextInt(9999);
	   switch(z)
	   {
	   case 0: return "0";
	   case 1: return "1";
	   case 2: return "001";
	   case 3: return "100";
	   case 4: return "03";
	   case 5: return "50";
	   case 6: return String.valueOf(zahl2);
	   case 7: return String.valueOf(zahl3);
	   case 8: return String.valueOf(zahl4);
	   default: return "70";
	   }
	   
   }
   public String getPostleitzahl()
   {
       int z = random.nextInt(10);
       switch(z)
	   {
	   case 0: return "86343"; // K�nigsbrunn
	   case 1: return "86199"; // Universit�tsviertel (Augsburg)
	   case 2: return "85521";   // Ottobrunn (M�nchen)
	   case 3: return "21039";   // Hamburg
	   case 4: return "28197";   // Bremen Altstadt
	   case 5: return "90522";   // Oberasbach b. N�rnberg
	   case 6: return "86399";   // Bobingen
	   case 7: return "50668";   // Bayental (K�ln)
	   case 8: return "60314";   // Berkersheim (Frankfurt am Main)
	   default: return "514";    // Bergisch Gladbach
	   }
   }
   public String getOrt(String plz)
   {
	   if(plz.equals("86343")) return "K�nigsbrunn";
	   if(plz.equals("86199")) return "Universit�tsviertel (Augsburg)";
	   if(plz.equals("85521")) return "Ottobrunn (M�nchen)";
	   if(plz.equals("21039")) return "Hamburg";
	   if(plz.equals("28197")) return "Bremen Altstadt";
	   if(plz.equals("90522")) return "Oberasbach b. N�rnberg";
	   if(plz.equals("86399")) return "Bobingen";
	   if(plz.equals("50668")) return "Bayental (K�ln)";
	   if(plz.equals("60314")) return "Berkersheim (Frankfurt am Main)";
	   if(plz.equals("514"))   return "Bergisch Gladbach";
	   return null;
   }
   public String getStrasse()
   {
	   int z = random.nextInt(20);
	   switch(z)
	   {
	   case 0: return "Fr�hlingsstra�e";
	   case 1: return "Sommerstra�e";
	   case 2: return "Herbststra�e";
	   case 3: return "Winterstra�e";
	   case 4: return "Blumenallee";
	   case 5: return "Rosenstra�e";
	   case 6: return "Pf�nderweg";
	   case 7: return "Leitnerstra�e";
	   case 8: return "Korbinianweg";
	   case 9: return "Wohlfahrtstra�e";
	   case 10: return "Kleeblattstra�e";
	   case 11: return "Hofgasse";
	   case 12: return "Veilchenweg";
	   case 13: return "Pfalzstra�e";
	   case 14: return "Tannenweg";
	   case 15: return "Mandelstra�e";
	   case 16: return "Kochstra�e";
	   case 17: return "Lillienalle";
	   case 18: return "Auguststra�e";
	   default: return "J�gerstra�e";
	   }
   }
   public String getHausnummer()
   {
	   int zahl = random.nextInt(999);
	   int zahl2 = random.nextInt(9999);
	   double r = random.nextDouble();
	   int buchstabe = random.nextInt(7);
	   if(r > 0.3) return String.valueOf(zahl2);
	   else
	   {
		   String w = String.valueOf(zahl);
		   if(buchstabe == 0) return w + "a";
		   if(buchstabe == 1) return w + "b";
		   if(buchstabe == 2) return w + "c";
		   if(buchstabe == 3) return w + "d";
		   if(buchstabe == 4) return w + "e";
		   if(buchstabe == 5) return w + "f";
		   else return w + "g";
	   }
   }
   public String getMarketingattribut()
   {
	   int z = random.nextInt(10);
	   switch(z)
	   {
	       case 0: return "Softskillseminar zu F�hrungsqualit�ten";
	       case 1: return "Softskillseminar zu Kommunikation";
	       case 2: return "Softskillseminar zu Teamarbeit";
	       case 3: return "Weiterbildung in Bedienung von Office";
	       case 4: return "Weiterbildung in Bedienung von SAP Software";
	       case 5: return "Weiterbildung in Bedienung von Expertensystemen";
	       case 6: return "Seminar �ber Umgangsformen";
	       case 7: return "Seminar �ber Verkauf von Produkten";
	       case 8: return "Seminar �ber Kontaktaufnahme zu Kunden";
	       default: return "Seminar �ber erfolgreiche Projektdurchf�hrung";
	   }
   }
   public String getDatum()
   {
	   int ziffer1 = random.nextInt(30)+1;
	   int ziffer2 = random.nextInt(12)+1;
	   int ziffer3 = random.nextInt(14)+2000;
	   return String.valueOf(ziffer1)+"."+String.valueOf(ziffer2)+"."+String.valueOf(ziffer3);
   }
   public String getAnlageLink()
   {
	   int z = random.nextInt(10);
	   switch(z)
	   {
	       case 0: return "http://www.uni-augsburg.de/Sopro/dateien/objektdiagramm.pdf";
	       case 1: return "http://www.uni-augsburg.de/Sopro/dateien/kontrakte.pdf";
	       case 2: return "http://www.uni-augsburg.de/Sopro/dateien/lifecycle.pdf";
	       case 3: return "http://www.uni-augsburg.de/Sopro/dateien/layout.pdf";
	       case 4: return "http://www.uni-augsburg.de/Sopro/dateien/Funktionsweise_D3.pdf";
	       case 5: return "http://www.uni-augsburg.de/Sopro/dateien/CRM_Orgchart_Help.pdf";
	       case 6: return "http://www.uni-augsburg.de/Sopro/dateien/Storyboard.pdf";
	       case 7: return "http://www.uni-augsburg.de/Sopro/dateien/Testdatengenerator_Konzept.pdf";
	       case 8: return "http://www.uni-augsburg.de/Sopro/dateien/Sequenzdiagramme.pdf";
	       default: return "http://www.uni-augsburg.de/Sopro/dateien/Datenbankmodel.pdf";
	   }
   }
   public String getOrganisation()
   {
	   int z = random.nextInt(20);
	   switch(z)
	   {
	   case 0: return "Lieferdienst Maier$Lieferant";
	   case 1: return "Brauerei Meltendorf & H�gel$Lieferant von Getr�nken";
	   case 2: return "Heins IT Services & %Financial Administration Serives$IT-Dienstleister";
	   case 3: return "Kupps & Kropps Unternehmensberatung$Berater";
	   case 4: return "Lieferdienstservice Jung & %Kelters frisches Obst und Gem�se$Lieferant von Obst und Gem�se";
	   case 5: return "Torstens Hardwareverleih Stark und ohne Grenzen$Hardware-Lieferant";
	   case 6: return "Lilianas selbstgestrickte Kleidung & %die qualitativ hochwertigste Arbeitskleidung$Kleider-Lieferant";
	   case 7: return "Massagen Klinsch & Jintsch$Wohlf�hl-Dienstleister";
	   case 8: return "Reisen ohne Stress mit Sonnenreisen Reiseb�ro$Dienstreisenorganisator";
	   case 9: return "Monteure Gerlt & Gurlt$Montage-Dienstleister";
	   case 10: return "Hahns speziell individuelle IT Serives & %Network simply Configuring$IT-Dienstleister";
	   case 11: return "Bruces Taxiservices & Audi Corporations$Fahrdienst";
	   case 12: return "Jahns Premiumbackwaren aller Art$Lieferant von Backwaren";
	   case 13: return "Autoh�ndler Torben mit Audi-Premiumautos & %BMW-Spitzensportw�gen$Autolieferant";
	   case 14: return "Franz perfekte IT-L�sung f�r jedes Problem & %nur die besten Softwarelizenzen$IT-Dienstleister";
	   case 15: return "Unternehmensberatung & IT-Beratung Koller$IT-Dienstleister & Berater";
	   case 16: return "Georges Personalfirma f�r Leiharbeiter$Personalverwalter";
	   case 17: return "Loreens mobiler Friseurservice & %selbst hergestellte Premiumkosmetikprodukte$Dienstleister";
	   case 18: return "B�cherei Zehnfu�s Exklusivtitel$B�cherverleiher";
	   default: return "Blumenmaries gro�e Auswahl an Blumen & %die exotischten Pflanzen aus dem Dschungel$Blumen und Pflanzen-Lieferant";
	   }
   }
   public String getBeziehungInformellKommentar()
   {
	   int z = random.nextInt(10);
	   switch(z)
	   {
	       case 0: return "spielen zusammen Tennis";
	       case 1: return "trinken oft zusammen Kaffee";
	       case 2: return "sind miteinander gut befreundet";
	       case 3: return "verstehen sich ausgezeichnet miteinander";
	       case 4: return "fahren gemeinsam in den Urlaub";
	       case 5: return "gehen �fters gemeinsam zum Golfen";
	       case 6: return "arbeiten gerne zusammen";
	       case 7: return "haben schon in vielen Projekten zusammen gearbeitet";
	       case 8: return "kennen sich schon lange und sind befreundet";
	       default: return "vertrauen sich gegenseitig";
	   }
   }
}

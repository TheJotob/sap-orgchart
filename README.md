# SAP_OrgChart #

## Was ist SAP_OrgChart? ##
SAP_OrgChart ist eine Webanwendung zur Visualisierung von Organigrammen aus SAP Datensätzen, zur Unterstützung der Vertriebsabteilung. Entstanden ist dieses Programm in Zuge des Softwareprojektes im Sommersemester 2015 an der Universität Augsburg im Auftrag von MHP.

## Installation ##
Zur Installation wird [Composer][1] und [git][2] benötigt.

### Download ###
Das git Repository muss geklont werden um den Code auf dem Computer bereitstellen zu können.
```sh
$ git clone https://www.bitbucket.org/TheJotob/sap-orgchart.git
```
### Installation der Abhängigkeiten ###
Das Programm basiert auf dem Zend Framework. Um dieses zu installieren genügt es sich in den Ordner zu begeben und mittels Composer die Abhängigkeiten zu installieren:
```sh
$ cd /root/of/application/
$ composer install
```

Nun sollte die Anwendung einsatzbereit sein. Viel Spaß mit dem OrgChart!

[1]:https://getcomposer.org/
[2]:https://git-scm.com/
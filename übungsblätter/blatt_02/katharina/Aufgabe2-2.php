<?php
#html-Teil: Überschriften, Layout
?>
	<h1>SoPro _ Blatt 2 _ Aufgabe 2<h1>
	<hr />
	<h2>Ansprechpartner<h2>
	<hr />

<?php
#Datum:
	echo date("D d.m.Y");
	echo " -- ";
	echo date("H:i:s");
	echo "<br /><br />";

#php-Teil: Tabelle
	$Variable = "Tabelle der Ansprechpartner:";
	echo $Variable;
	echo "<br />";
	
#Verbindungsaufbau:
	$db_link = mysqli_connect('localhost', 'root', '', 'sopro');
	$sql = "SELECT * FROM ansprechpartner";
	$db_erg = mysqli_query( $db_link, $sql );
	if(!$db_erg)
	{
  		exit("Verbindungsfehler: ".mysqli_connect_error());
	}

#Tabelle ausgeben
	$sql = "SELECT * FROM ansprechpartner";
 
	$db_erg = mysqli_query( $db_link, $sql );
	if ( ! $db_erg )
	{
	  die('Ungültige Abfrage: ' . mysqli_error());
	}
 
	echo '<table border="1">';
	while ($zeile = mysqli_fetch_array( $db_erg, MYSQL_ASSOC))
	{
		echo "<tr>";
		echo "<td>".$zeile['id']."</td>";
		echo "<td>".$zeile['anrede']."</td>";
		echo "<td>".$zeile['titel']."</td>";
		echo "<td>".$zeile['vorname']."</td>";
		echo "<td>".$zeile['nachname']."</td>";
		echo "<td>".$zeile['geschlecht']."</td>";
		echo "<td>".$zeile['sprache']."</td>";
		echo "</tr>";
	}
	echo "</table>";
	mysqli_free_result($db_erg);
?>
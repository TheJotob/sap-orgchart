<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
  <head>
    <title>Daten eines Ansprechpartners auslesen</title>
    <h1>Daten eines Ansprechpartners auslesen</h1>
   
  </head>
  <body>
    <?php
     require_once ('konfiguration.php');
     $db_link = mysqli_connect (
                     MYSQL_HOST, 
                     MYSQL_BENUTZER, 
                     MYSQL_KENNWORT, 
                     MYSQL_DATENBANK
                    );

     if ( $db_link )
     {
         echo 'Verbindung erfolgreich: ';
         print_r( $db_link);
     }
     else
     {
         die('keine Verbindung m�glich: ' . mysqli_error($db_link));
     }
 
     $sql = "SELECT * FROM ansprechpartner";
 
     $db_erg = mysqli_query( $db_link, $sql );
     if ( ! $db_erg )
     {
       die('Ung�ltige Abfrage: ' . mysqli_error($db_erg));
     }
 
     echo '<table border="1">';
     while ($zeile = mysqli_fetch_array( $db_erg, MYSQL_ASSOC))
     {
       echo "<tr>";
       echo "<td>". $zeile['ID'] . "</td>";
       echo "<td>". $zeile['Titel'] . "</td>";
       echo "<td>". $zeile['Akadem. Titel'] . "</td>";
       echo "<td>". $zeile['Vorname'] . "</td>";
       echo "<td>". $zeile['Nachname'] . "</td>";
       echo "<td>". $zeile['Geschlecht'] . "</td>";
       echo "<td>". $zeile['Sprache'] . "</td>";
       echo "<td>". $zeile['Account_ID'] . "</td>";
       echo "<td>". $zeile['Kommunikation_ID'] . "</td>";
       echo "</tr>";
     }
     echo "</table>";
 
     mysqli_free_result( $db_erg );
    ?>
  </body>
</html>
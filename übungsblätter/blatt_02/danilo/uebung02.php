<?php 

#Verbindung zur Datenbank aufbauen
$db_link = mysqli_connect('localhost', 'root', '', 'testdb');
$sql = "SELECT * FROM ansprechpartner";
$db_erg = mysqli_query( $db_link, $sql );
if(!$db_erg)
{
  exit("Verbindungsfehler: ".mysqli_connect_error());
}



#Ausgabe als Tabelle

#1.Beschriftung der Tabelle
echo '<table>';
echo "<tr>";						
echo "<td> ID: </td>";
echo "<td> Details: </td>";
echo "<td> Beziehung: </td>";
echo "<td> Marketingattribute: </td>";
echo "<td> Notizen: </td>";
echo "<td> Anlagen(Nicht zwingend): </td>";
echo "</tr>";
#2.Ausgabe der Tabelleninhalte aus DB
while ($zeile = mysqli_fetch_array( $db_erg, MYSQL_ASSOC))
{
	echo "<tr>";
	echo "<td>". $zeile['id'] . "</td>";
	echo "<td>". $zeile['Details'] . "</td>";
	echo "<td>". $zeile['Beziehung'] . "</td>";
	echo "<td>". $zeile['Marketingattribute'] . "</td>";
	echo "<td>". $zeile['Notizen'] . "</td>";
	#Anlagen nicht zwingend!
	if($zeile['Anlagen']==null)
	{				
		echo "<td> NULL! </td>";
	}
	else
	{
		echo "<td>". $zeile['Anlagen'] . "</td>";
	}
	echo "</tr>";
}
echo "</table>";

mysqli_free_result( $db_erg );



?>

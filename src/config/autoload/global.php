<?php
return array(
		'db' => array(
				'driver' => 'Pdo',
				'dsn' => 'mysql:dbname=sap_orgchart;host=localhost',
				'driver_options' => array(
						PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\'',
						PDO::MYSQL_ATTR_LOCAL_INFILE => true)),
		'service_manager' => array(
				'factories' => array(
						'Zend\Db\Adapter\Adapter' => function ($serviceManager) {
							$adapterFactory = new Zend\Db\Adapter\AdapterServiceFactory();
							$adapter = $adapterFactory->createService($serviceManager);
							
							\Zend\Db\TableGateway\Feature\GlobalAdapterFeature::setStaticAdapter($adapter);
							
							return $adapter;
						})));

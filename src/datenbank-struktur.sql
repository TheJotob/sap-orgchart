-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Erstellungszeit: 07. Jul 2015 um 20:45
-- Server-Version: 5.6.24
-- PHP-Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Datenbank: `sap_orgchart`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `connections`
--

CREATE TABLE IF NOT EXISTS `connections` (
  `Contact_ID1` int(11) NOT NULL,
  `Contact_ID2` int(11) NOT NULL,
  `Comment` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `Department_Name` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `Function_Name` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `Formal` tinyint(1) NOT NULL,
  `Relationship_Type` varchar(3) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `EMails` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `Addresses` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `Attachments` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `MarketingAttrs` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `Memos` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `Phone` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `contacts`
--

CREATE TABLE IF NOT EXISTS `contacts` (
  `ID` int(11) NOT NULL,
  `Firstname` varchar(70) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `Lastname` varchar(70) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `Title_Key` varchar(4) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `Academic_Title` varchar(15) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `Type` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `Name1` varchar(70) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `Name2` varchar(70) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `EMails` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `Addresses` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `Attachments` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `MarketingAttrs` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `Memos` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `Phone` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `search_index`
--

CREATE TABLE IF NOT EXISTS `search_index` (
  `Contact_ID` int(11) NOT NULL,
  `Field` varchar(150) COLLATE utf8_bin NOT NULL,
  `Value` varchar(150) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`ID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php
namespace Application\View;

use Zend\Form\Form;
use Zend\Form\Element;

/**
 * Upload Form for new JSON Data
 * @author Joachim Eckerlin <joachim.eckerlin@gmail.com>
 *
 */
class JSONUploadForm extends Form {

	/**
	 * Constructor
	 * Initializes and generates form
	 * @param string $name
	 * @param string[] $options
	 */
	public function __construct($name = null, $options = null) {
		parent::__construct('jsonUpload');
		
		$this->setAttributes(array(
				'class' => 'form-inline'
		));
		$this->addElements();
	}

	/**
	 * Adds form-elements to form
	 */
	private function addElements() {
		$this->add(array(
				'type' => 'Zend\Form\Element\File',
				'name' => 'filJsonImport',
				'label' => 'JSON Import',
				'attributes' => array(
						'id' => 'filJsonImport',
						'class' => '',
						'required' => 'required')));
		
		$this->add(array(
				'type' => 'Zend\Form\Element\Submit',
				'name' => 'btnJsonImportSubmit',
				'attributes' => array(
						'id' => 'btnJsonImportSubmit',
						'class' => 'btn btn-default',
						'value' => 'Import!')));
	}
}
<?php
namespace Application\Model\Api;

/**
 * Class to get Nodes in API
 * @author Joachim Eckerlin <joachim.eckerlin@gmail.com>
 *
 */
class Nodes {
	private $table;
	private $childLevels = 0;
	private $nodeIds = array();

	/**
	 * Constructor
	 * @param Zend\Db\TableGateway $table
	 */
	public function __construct($table) {
		$this->table = $table;
	}

	/**
	 * Method to get children of node
	 * @param Int[] $node_ids        	
	 * @param Int $level        	
	 */
	public function getChildren($node_ids, $level) {
		if(count($node_ids) == 0)
			return array();
		
		$children = $this->table->fetchChildren($node_ids);
		if(!is_array($node_ids))
			$node_ids = array($node_ids);
		
		$this->nodeIds = array_merge($this->nodeIds, $node_ids);
		
		$objects = array();
		$node_ids = array();

		foreach ( $children as $child ) {
			$objects [] = $child->getData();
			$node_ids [] = $child->id;
		}

		if($level > 1)
			return array_merge($objects, $this->getChildren($node_ids, $level - 1));
		else {
			$this->nodeIds = array_merge($this->nodeIds, $node_ids);
			return $objects;
		}
	}

	/**
	 * Method to get parent of node
	 *
	 * @param Int[] $node_id        	
	 * @param Int $level        	
	 */
	public function getParents($node_id, $level) {
		$parent = $this->table->fetchParent($node_id);

		if(is_object($parent)) {
			$this->childLevels++;
			if($level > 1)
				return $this->getParents($parent->id, $level-1);
			else 
				return array_merge(array($this->table->getContact($parent->id)->getData()), $this->getChildren(array($parent->id), $this->childLevels));
		} else 
			return array_merge(
					array($this->table->getContact($node_id)->getData()),
					$this->getChildren($node_id, $this->childLevels));
	}
	
	/**
	 * Returns all Node IDs from previous requests.
	 */
	public function getNodeIds() {
		return array_values(array_unique($this->nodeIds));
	}
}

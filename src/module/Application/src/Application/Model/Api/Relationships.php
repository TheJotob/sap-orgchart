<?php
namespace Application\Model\Api;

/**
 * Class for getting the relationships in API
 * @author Joachim Eckerlin <joachim.eckerlin@gmail.com>
 *
 */
class Relationships {
	private $table;
	
	/**
	 * Constructor
	 * @param Zend\Db\TableGateway $table
	 */
	public function __construct($table) {
		$this->table = $table;
	}
	
	/**
	 * Get Connections for $node_ids
	 * @param Int[] $node_ids
	 */
	public function getRelationships($node_ids) {
		$objects = array();
		$relationships = $this->table->fetchAll($node_ids);
		
		foreach ($relationships as $relationship)
			$objects[] = $relationship->getData();
		
		return $objects;
	}
}

<?php
namespace Application\Model;

/**
 * Class for performing action on a single SearchAttribute.
 * Stores Data of search results.
 * @author Joachim Eckerlin <joachim.eckerlin@gmail.com>
 *
 */
class SearchAttribute {
	public $id, $field, $value;

	/**
	 * Saves data from Array into Object
	 * @param Array $data
	 */
	public function exchangeArray($data) {
		$this->id = $data ['Contact_ID'];
		$this->field = $data ['Field'];
		$this->value = $data ['Value'];
		
		if(!empty($data ['Firstname']))
			$this->name = $data ['Firstname'] . " " . $data ['Lastname'];
		else if(!empty($data ['Name1']))
			$this->name = $data ['Name1'] . " " . $data ['Name2'];
	}
}

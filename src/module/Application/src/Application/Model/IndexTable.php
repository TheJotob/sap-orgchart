<?php
namespace Application\Model;

/**
 * Class to interact with search index table in database
 * @author Joachim Eckerlin <joachim.eckerlin@gmail.com>
 *
 */
class IndexTable extends DatabaseTable {
	protected $tableName = 'search_index';

	/**
	 * Search in Database for query
	 * @param String $query
	 * @return Zend\Db\ResultSet
	 */
	public function search($query) {
		$select = $this->tableGateway->getSql()
			->select();
		$select->join(array(
				'c' => 'contacts'), 'search_index.Contact_ID = c.ID')
			->where('Value LIKE "%' . $query . '%"')
			->group('search_index.Contact_ID');
		$resultSet = $this->tableGateway->selectWith($select);
		return $resultSet;
	}

	/**
	 * Writes search attributes into CSV for later importing in Database
	 * @param Array $attributes
	 */
	public function add($attributes) {
		$csv = $this->getCsvFile();
		$this->quantity += count($attributes);
		
		foreach($attributes as $attribute)
			fputcsv($csv, $attribute, ',', '\'');
	}
}

<?php
namespace Application\Model;

use Zend\Db\TableGateway\TableGateway;
use Application\Model\Relationship;

/**
 * Class to interact with the connections Table in the Database.
 * @author Joachim Eckerlin <joachim.eckerlin@gmail.com>
 *
 */
class RelationshipsTable extends DatabaseTable {
	protected $tableName = 'connections';

	/**
	 * Returns all relationships, that have at least one of the nodes in node_ids in it
	 * @param Int[] $node_ids
	 * @return \Zend\Db\ResultSet\ResultSet>
	 */
	public function fetchAll($node_ids) {
		$select = $this->tableGateway->getSql()->select();
		
		$where = new \Zend\Db\Sql\Where();
		$where	->in('Contact_ID1', $node_ids)
				->or
				->in('Contact_ID2', $node_ids);
		
		$select->where($where);
		$resultSet = $this->tableGateway->selectWith($select);
		return $resultSet;
	}

	/**
	 * Saves a relationship to the CSV-file for later upload in database
	 * @param Relationship $relationship
	 */
	public function saveConnection(Relationship $relationship) {
		$this->quantity++;
		$csv = $this->getCsvFile();
		
		fputcsv($csv, $relationship->getDatabaseArray(), ',', '\'');
	}
}

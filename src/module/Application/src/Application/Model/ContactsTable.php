<?php
namespace Application\Model;

use Application\Model\Contact;

/**
 * Class to interact with contacts table in database
 * @author Joachim Eckerlin <joachim.eckerlin@gmail.com>
 *
 */
class ContactsTable extends DatabaseTable {
	protected $tableName = 'contacts';

	/**
	 * Get every contact from database
	 * @return unknown
	 */
	public function fetchAll() {
		$resultSet = $this->tableGateway->select();
		return $resultSet;
	}

	/**
	 * Get Children of node_ids
	 * @param Int[] $node_ids
	 * @return Zend\Db\ResultSet
	 */
	public function fetchChildren($node_ids) {
		$select = $this->tableGateway->getSql()
			->select();
		$select->columns(array())
			->join(array(
				'conns' => 'connections'), 'conns.Contact_ID1 = contacts.ID')
			->join(array(
				'contact2' => 'contacts'), 'conns.Contact_ID2 = contact2.ID')
			->where(array(
				'conns.Contact_ID1' => $node_ids,
				'conns.Formal' => '1'));
		$resultSet = $this->tableGateway->selectWith($select);
		return $resultSet;
	}

	/**
	 * Get parent of $node_id
	 * @param Int $node_id
	 * @return Application\Model\Contact
	 */
	public function fetchParent($node_id) {
		$select = $this->tableGateway->getSql ()->select ();
		$select->columns (array())
			->join(array('conns' => 'connections'), 'conns.Contact_ID2 = contacts.ID')
			->join(array('contact1' => 'contacts'), 'conns.Contact_ID1 = contact1.ID')
			->where(array(
				'conns.Contact_ID2' => $node_id,
				'conns.Formal' => '1'
			));
			$parent_node = $this->tableGateway->selectWith($select);
			return $parent_node->current();
	}
	
	/**
	 * Get contact by ID
	 * @param Int $id
	 * @return Application\Model\Contact
	 */
	public function getContact($id) {
		$id = (int) $id;
		
		$select = $this->tableGateway->getSql()->select();
		$select->join('connections', 'connections.Contact_ID2 = contacts.ID')
			->where(array('contacts.ID' => $id));
		
		$rowset = $this->tableGateway->selectWith($select);
		if($row = $rowset->current())
			return $row;
		else
			return $this->tableGateway->select(array(
				'id' => $id))->current();
	}

	/**
	 * Saves contact to CSV for later saving in database
	 * @param Contact $contact
	 */
	public function saveContact(Contact $contact) {
		$this->quantity++;
		$csv = $this->getCsvFile();
		
		$dbArray = array(
				'ID' => $contact->id,
				'Firstname' => $contact->firstname,
				'Lastname' => $contact->lastname,
				'Title_Key' => $contact->title_key,
				'Academic_Title' => $contact->academic_title,
				'Type' => $contact->type,
				'Name1' => $contact->name1,
				'Name2' => $contact->name2,
				'EMails' => $contact->strEMails,
				'Addresses' => $contact->strAddresses,
				'Attachments' => $contact->strAttachments,
				'MarketingAttrs' => $contact->strMarketingAttrs,
				'Memos' => $contact->strMemos,
				'Phone' => $contact->strPhone);
		
		fputcsv($csv, $dbArray, ',', '\'');
	}
}

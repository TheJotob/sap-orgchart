<?php

namespace Application\Model;

/**
 * Class to import Contacts and Connections into Database
 * @author Joachim Eckerlin <joachim.eckerlin@gmail.com>
 *
 */
class Importer {
	private $contactsJson, $relationshipsJson;
	private $relationshipsTable, $contactsTable, $searchIndexTable;
	private $searchIndex;
	
	private $start;
	public $result;

	/**
	 * Constructor.
	 * Performs Import.
	 * @param String $json
	 * @param String[] $tables
	 */
	public function __construct($json, $tables) {
		$this->start = microtime(true);
		$this->contactsJson = $json['kontakte'];
		$this->relationshipsJson = $json['beziehungen'];
		
		$json = null;
		
		$this->relationshipsTable = $tables ['relationships'];
		$this->contactsTable = $tables ['contacts'];
		$this->searchIndexTable = $tables ['search_index'];
		
		// Clear Tables
		$this->searchIndexTable->deleteAll ();
		$this->relationshipsTable->deleteAll ();
		$this->contactsTable->deleteAll ();
		
		// Prepare Data
		$this->importContacts ();
		$this->importConnections ();
		
		// Write Data into Database
		$this->relationshipsTable->doInsert();
		$this->contactsTable->doInsert();
		$this->searchIndexTable->doInsert();
		
		// Prepare Data for ouput
		$this->result = array(
				'success' => true,
				'relationships' => $this->relationshipsTable->getQuantity(),
				'contacts' => $this->contactsTable->getQuantity(),
				'search_index' => $this->searchIndexTable->getQuantity(),
				'duration' => (microtime(true) - $this->start),
		);
	}

	/**
	 * Sub-routine to import contacts
	 */
	private function importContacts() {
		foreach ($this->contactsJson as $data ) {
			$contact = new Contact ();
			$contact->exchangeArrayFromJson($data);

			$this->contactsTable->saveContact($contact);
			$this->searchIndexTable->add($contact->getIndex());
		}
	}

	/**
	 * Sub-routine to import relationships
	 */
	private function importConnections() {
		foreach ( $this->relationshipsJson as $data ) {
			$connection = new Relationship ();
			$connection->exchangeArrayFromJson($data);

			$this->relationshipsTable->saveConnection($connection);
			$this->searchIndexTable->add($connection->getIndex());
		}
	}
}

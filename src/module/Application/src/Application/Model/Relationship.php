<?php
namespace Application\Model;

/**
 * Class to perform actions on single relationships.
 * Manages data of relationship.
 * @author Joachim Eckerlin <joachim.eckerlin@gmail.com>
 *
 */
class Relationship {
	public $contact1, $contact2;
	public $comment;
	public $department, $function;
	public $type;
	public $emails = array(), $addresses = array(), $attachments = array(), $marketingAttrs = array(), $memos = array(), $phone = array();
	public $strEMails, $strAttachments, $strAddresses, $strMarketingAttrs, $strMemos, $strPhone;
	
	private $dbArray = array(), $index = array();

	// Saves data from array into object
	public function exchangeArray($data) {
		$this->contact1 = (!empty($data ['Contact_ID1'])) ? $data ['Contact_ID1'] : '';
		$this->contact2 = (!empty($data ['Contact_ID2'])) ? $data ['Contact_ID2'] : '';
		$this->comment = (!empty($data ['Comment'])) ? $data['Comment'] : '';
		$this->department = (!empty($data ['Department_Name'])) ?  $data['Department_Name'] : '';
		$this->function = (!empty($data ['Function_Name'])) ? $data['Function_Name'] : '';
		$this->formal = ($data['Formal'] == '1') ? true : false;
		$this->type = substr($data['Relationship_Type'], 0, 3);
		
		if(!empty($data ['notiz'])) {
			$this->strMemos = serialize($data ['notiz']);
			$this->memos = $data ['notiz'];
		} else if(!empty($data ['Memos'])) {
			$this->strMemos = $data ['Memos'];
			$this->memos = unserialize($data ['Memos']);
		} else {
			$this->strMemos = '';
			$this->memos = array();
		}
		
		if(!empty($data ['marketingattribut'])) {
			$this->strMarketingAttrs = serialize($data ['marketingattribut']);
			$this->marketingAttrs = $data ['marketingattribut'];
		} else if(!empty($data ['MarketingAttrs'])) {
			$this->strMarketingAttrs = $data ['MarketingAttrs'];
			$this->marketingAttrs = unserialize($data ['MarketingAttrs']);
		} else {
			$this->strMarketingAttrs = '';
			$this->marketingAttrs = array();
		}
		
		if(!empty($data ['e-Mail'])) {
			$this->strEMails = serialize($data ['e-Mail']);
			$this->emails = $data ['e-Mail'];
		} else if(!empty($data ['EMails'])) {
			$this->strEMails = $data ['EMails'];
			$this->emails = unserialize($data ['EMails']);
		} else {
			$this->strEMails = '';
			$this->emails = array();
		}
		
		if(!empty($data ['anlage'])) {
			$this->strAttachments = serialize($data ['anlage']);
			$this->attachments = $data ['anlage'];
		} else if(!empty($data ['Attachments'])) {
			$this->strAttachments = $data ['Attachments'];
			$this->attachments = unserialize($data ['Attachments']);
		} else {
			$this->strAttachments = '';
			$this->attachments = array();
		}
		
		if(!empty($data ['adresse'])) {
			$this->strAddresses = serialize($data ['adresse']);
			$this->addresses = $data ['adresse'];
		} else if(!empty($data ['Addresses'])) {
			$this->strAddresses = $data ['Addresses'];
			$this->addresses = unserialize($data ['Addresses']);
		} else {
			$this->strAddresses = '';
			$this->addresses = array();
		}
		
		if(!empty($data ['nummer'])) {
			$this->strPhone = serialize($data ['nummer']);
			$this->phone = $data ['nummer'];
		} else if(!empty($data ['Phone'])) {
			$this->strPhone = $data ['Phone'];
			$this->phone = unserialize($data ['Phone']);
		} else {
			$this->strPhone = '';
			$this->phone = array();
		}
	}
	
	// Saves data from array into different mapped arrays for import
	public function exchangeArrayFromJson($data) {
		$this->contact2 = $data['Partner2_ID'];
		
		// Map Data for DB
		$this->dbArray = array(
				'Contact_ID1' => $data ['Partner1_ID'],
				'Contact_ID2' => $data ['Partner2_ID'],
				'Comment' => (!empty($data ['Comment'])) ? $data['Comment'] : null,
				'Department_Name' => $data ['Department_Name'],
				'Function_Name' => $data ['Function_Name'],
				'Formal' => (substr($data['Relationship_Type'], 0, 1) == '1') ? true : false,
				'Relationship_Type' => substr($data['Relationship_Type'], 1, 3),
				'EMails' => serialize($data ['e-Mail']),
				'Addresses' => serialize($data ['adresse']),
				'Attachments' => serialize($data ['anlage']),
				'MarketingAttrs' => serialize($data ['marketingattribut']),
				'Memos' => serialize($data ['notiz']),
				'Phone' => serialize($data ['nummer']));
		
		// Build Search Index
		$this->addToIndex('Abteilung', $data['Department_Name']);
		$this->addToIndex('Funktion', $data['Function_Name']);
		
		foreach($data['e-Mail'] as $email)
			$this->addToIndex('E-Mail', $email['E-Mail']);
				
		foreach($data['anlage'] as $attachment)
			$this->addToIndex('Anlage', $attachment['Attachement-Name']);
					
		foreach($data['notiz'] as $memo)
			$this->addToIndex('Notiz', $memo['Notiz']);
						
		foreach($data ['marketingattribut'] as $marketingAttr)
			$this->addToIndex('Marketing Attribut', $marketingAttr['Attribute Set']);

		foreach ($data ['nummer'] as $phone)
			$this->addToIndex($phone ['Number_Type'], "+" . $phone ['Countryiso'] . $phone ['Number'] . $phone ['Extension']);

		foreach($data ['adresse'] as $address )
			$this->addToIndex('Adresse', $address ['Street'] . " " . $address ['House No.'] . " " . $address ['Post Code'] . " " . $address ['City'] . " " . $address ['Country']);
	}
	
	// Adds Data to search index array
	private function addToIndex($field, $value) {
		$this->index[] = array(
				'Contact_ID' => $this->contact2,
				'Field' => $field,
				'Value' => $value
		);
	}

	// Returns Data as Array
	public function getData() {
		return array(
				'Partner1_ID' => $this->contact1,
				'Partner2_ID' => $this->contact2,
				'Comment' => $this->comment,
				'Formal' => ($this->formal) ? 1 : 0,
				'Directed' => substr($this->type, 0, 1),
				'Color' => substr($this->type, -2),
				'notiz' => $this->memos,
				'e-Mail' => $this->emails,
				'nummer' => $this->phone,
				'adresse' => $this->addresses,
				'anlage' => $this->attachments,
				'nummer' => $this->phone);
	}
	
	// Returns data mapped for database
	public function getDatabaseArray() {
		return $this->dbArray;
	}
	
	// Returns search index for Relationship
	public function getIndex() {
		return $this->index;
	}
}

<?php
namespace Application\Model;

use Zend\Db\TableGateway\TableGateway;

/**
 * Base class for interacting with database tables.
 * @author Joachim Eckerlin <joachim.eckerlin@gmail.com>
 *
 */
abstract class DatabaseTable {
	
	protected $tableGateway;
	protected $tableName;
	protected $data = array();
	protected $csvData = null;
	protected $quantity = 0;
	
	/**
	 * Constructor.
	 * Initializes connection.
	 * @param TableGateway $tableGateway
	 */
	public function __construct(TableGateway $tableGateway) {
		$this->tableGateway = $tableGateway;
	}
	
	/**
	 * Generates a Data-Load-Query and sends it to database
	 */
	public function doInsert() {
		$csv = $this->csvData;
		$metaData = stream_get_meta_data($this->csvData);
		$filename = $metaData['uri'];

		$insert = 'LOAD DATA LOCAL INFILE "' . addslashes($filename) . '" INTO TABLE ' . $this->tableName . ' CHARACTER SET utf8 FIELDS TERMINATED BY "," ENCLOSED BY "\'" LINES TERMINATED BY "\n"';
		$this->tableGateway->getAdapter()->driver->getConnection()->execute($insert);

		fclose($this->csvData);
	
		return count($this->data);
	}
	
	/**
	 * Truncates current table.
	 */
	public function deleteAll() {
		$truncate = "TRUNCATE TABLE " . $this->tableName;
		$this->tableGateway->getAdapter()->driver->getConnection()->execute($truncate);
	}
	
	/**
	 * Getter for number of records
	 * @return number quantity
	 */
	public function getQuantity() {
		return $this->quantity;
	}
	
	/**
	 * Returns Handler for CSV File for current table
	 * @return resource
	 */
	protected function getCsvFile() {
		if(is_null($this->csvData))
			$this->csvData = tmpfile();
		
		return $this->csvData;
	}
}

<?php
namespace Application\Model;

/**
 * Class to represent a contact or node. Saves data and maps them for requests
 * @author Joachim Eckerlin <joachim.eckerlin@gmail.com>
 *
 */
class Contact {
	public $id;
	public $firstname, $lastname, $title_key, $academic_title;
	public $function, $department;
	public $type, $name1, $name2;
	public $emails = array(), $addresses=array(), $attachments=array(), $marketingAttrs=array(), $memos=array(), $phone=array();
	public $strEMails = '', $strAttachments = '', $strAddresses = '', $strMarketingAttrs = '', $strMemos = '', $strPhone = '';

	/**
	 * Saves Data from Array into Object
	 * 
	 * @param String[] $data        	
	 */
	public function exchangeArray($data) {
		$this->id = (!empty($data ['ID'])) ? $data ['ID'] : '';
		$this->firstname = (!empty($data ['Firstname'])) ? $data ['Firstname'] : '';
		$this->lastname = (!empty($data ['Lastname'])) ? $data ['Lastname'] : '';
		$this->title_key = (!empty($data ['Title_Key'])) ? $data ['Title_Key'] : '';
		$this->academic_title = (!empty($data ['Academic_Title'])) ? $data ['Academic_Title'] : '';
		$this->type = (!empty($data ['Type'])) ? $data ['Type'] : '';
		$this->name1 = (!empty($data ['Name1'])) ? $data ['Name1'] : '';
		$this->name2 = (!empty($data ['Name2'])) ? $data ['Name2'] : '';
		$this->department = (!empty($data ['Department_Name'])) ? $data ['Department_Name'] : '';
		$this->function = (!empty($data ['Function_Name'])) ? $data ['Function_Name'] : '';
		
		if(!empty($data ['Memos'])) {
			$this->strMemos = $data ['Memos'];
			$this->memos = unserialize($data ['Memos']);
		}
		
		if(!empty($data ['MarketingAttrs'])) {
			$this->strMarketingAttrs = $data ['MarketingAttrs'];
			$this->marketingAttrs = unserialize($data ['MarketingAttrs']);
		}
		
		if(!empty($data ['EMails'])) {
			$this->strEMails = $data ['EMails'];
			$this->emails = unserialize($data ['EMails']);
		}
		
		if(!empty($data ['Attachments'])) {
			$this->strAttachments = $data ['Attachments'];
			$this->attachments = unserialize($data ['Attachments']);
		}
		
		if(!empty($data ['Addresses'])) {
			$this->strAddresses = $data ['Addresses'];
			$this->addresses = unserialize($data ['Addresses']);
		}
		
		if(!empty($data ['Phone'])) {
			$this->strPhone = $data ['Phone'];
			$this->phone = unserialize($data ['Phone']);
		}
	}
	
	/**
	 * Saves data from JSON in instance of this class
	 * @param String[] $data
	 */
	public function exchangeArrayFromJson($data) {
		$this->id = (!empty($data ['Contact_ID'])) ? $data ['Contact_ID'] : null;
		$this->firstname = (!empty($data ['Firstname'])) ? $data ['Firstname'] : null;
		$this->lastname = (!empty($data ['Lastname'])) ? $data ['Lastname'] : null;
		$this->title_key = (!empty($data ['Title_Key'])) ? $data ['Title_Key'] : null;
		$this->academic_title = (!empty($data ['Academic_Title'])) ? $data ['Academic_Title'] : null;
		$this->type = (!empty($data ['Type'])) ? $data ['Type'] : null;
		$this->name1 = (!empty($data ['Name1'])) ? $data ['Name1'] : null;
		$this->name2 = (!empty($data ['Name2'])) ? $data ['Name2'] : null;
		
		if(!empty($data ['notiz'])) {
			$this->strMemos = serialize($data ['notiz']);
			$this->memos = $data ['notiz'];
		}
		
		if(!empty($data ['marketingattribut'])) {
			$this->strMarketingAttrs = serialize($data ['marketingattribut']);
			$this->marketingAttrs = $data ['marketingattribut'];
		}
		
		if(!empty($data ['e-Mail'])) {
			$this->strEMails = serialize($data ['e-Mail']);
			$this->emails = $data ['e-Mail'];
		}
		
		if(!empty($data ['anlage'])) {
			$this->strAttachments = serialize($data ['anlage']);
			$this->attachments = $data ['anlage'];
		}
		
		if(!empty($data ['adresse'])) {
			$this->strAddresses = serialize($data ['adresse']);
			$this->addresses = $data ['adresse'];
		}
		
		if(!empty($data ['nummer'])) {
			$this->strPhone = serialize($data ['nummer']);
			$this->phone = $data ['nummer'];
		}
	} 

	/**
	 * Method to get all searcheable fields with Field-Name and Value
	 * 
	 * @return String[][] $search_index
	 */
	public function getIndex() {
		$index = array();
		
		if($this->name1 != "")
			$index[] = array(
						'Contact_ID' => $this->id,
						'Field' => 'Name',
						'Value' => $this->name1 . " " . $this->name2);
		else
			$index[] = array(
						'Contact_ID' => $this->id,
						'Field' => 'Name',
						'Value' => $this->firstname . " " . $this->lastname);
		
		foreach($this->emails as $email)
			$index[] = array(
				'Contact_ID' => $this->id,
				'Field' => 'E-Mail',
				'Value' => $email['E-Mail']);

		foreach($this->attachments as $attachment)
			$index[] = array(
					'Contact_ID' => $this->id,
					'Field' => 'Anlage',
					'Value' => $attachment['Attachement-Name']
			);
			
		foreach($this->memos as $memo)
			$index[] = array(
					'Contact_ID' => $this->id,
					'Field' => 'Notiz',
					'Value' => $memo['Notiz']
			);
			
		foreach($this->marketingAttrs as $marketingAttr)
			$index[] = array(
					'Contact_ID' => $this->id,
					'Field' => 'Marketing Attribut',
					'Value' => $marketingAttr['Attribute Set']
			);

		foreach ($this->phone as $phone)
			$index [] = array(
					'Contact_ID' => $this->id,
					'Field' => $phone ['Number_Type'],
					'Value' => "+" . $phone ['Countryiso'] . $phone ['Number'] . $phone ['Extension']);

		foreach ( $this->addresses as $address )
			$index [] = array(
					'Contact_ID' => $this->id,
					'Field' => 'Address',
					'Value' => $address ['Street'] . " " . $address ['House No.'] . " " . $address ['Post Code'] . " " . $address ['City'] . " " . $address ['Country']);

		return $index;
	}

	/**
	 * Method to get all Data as Array
	 * 
	 * @param boolean (optional) $details
	 * @return String[]
	 */
	public function getData($details = true) {
		if($details)
			return array(
					'Contact_ID' => $this->id,
					'Firstname' => $this->firstname,
					'Lastname' => $this->lastname,
					'Function_Name' => $this->function,
					'Department_Name' => $this->department,
					'Name1' => $this->name1,
					'Name2' => $this->name2,
					'Title_Key' => $this->title_key,
					'Academic_Title' => $this->academic_title,
					'Type' => $this->type,
					'e-Mail' => $this->emails,
					'addresses' => $this->addresses,
					'attachments' => $this->attachments,
					'memos' => $this->memos,
					'nummer' => $this->phone);
		else
			return array(
					'Contact_ID' => $this->id,
					'Firstname' => $this->firstname,
					'Lastname' => $this->lastname,
					'Name1' => $this->name1,
					'Name2' => $this->name2,
					'Title_Key' => $this->title_key,
					'Academic_Title' => $this->academic_title,
					'Type' => $this->type);
	}
}

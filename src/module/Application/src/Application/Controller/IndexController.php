<?php
namespace Application\Controller;

use Application\View\JSONUploadForm;
use Application\Model\Importer;
use Zend\View\Model\ViewModel;

/**
 * Controller for all the pages (Index, Import and Print)
 * @author Joachim Eckerlin <joachim.eckerlin@gmail.com>
 *
 */
class IndexController extends ApplicationController {

	// Shows Main Page
	public function indexAction() {
		return new ViewModel();
	}

	// Shows JSON-Importer Upload Page
	public function importAction() {
		// Prepare Upload Form
		$form = new JSONUploadForm();
		$request = $this->getRequest();
		
		// If Page is called with Post-Data (after sending form)
		if($request->isPost()) {
			$files = $request->getFiles()
				->toArray();
			$data = $request->getPost()
				->toArray();
			
			// Merge data of uploaded files and POST informations and write them back to form fields
			$post = array_merge_recursive($files, $data);
			$form->setData($post);

			if($form->isValid()) {
				// Get Data from Uploaded JSON File and start Importer
				$data = $form->getData();
				$json = json_decode(file_get_contents($data ['filJsonImport'] ['tmp_name']), true);
				
				// Start import
				$importer = new Importer($json, array(
						'relationships' => $this->getRelationshipsTable(),
						'contacts' => $this->getContactsTable(),
						'search_index' => $this->getSearchIndexTable()));
				
				// Display Import Page 
				return new ViewModel(array_merge(
						$importer->result, 
						array(
							'form' => $form
						)));
			}
		}
		
		// Shows only form if nothing was imported
		return new ViewModel(array(
				'form' => $form,
				'success' => false
		));
	}
}

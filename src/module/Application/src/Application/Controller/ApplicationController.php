<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;

/**
 * Base class for controllers in this project
 * @author Joachim Eckerlin <joachim.eckerlin@gmail.com>
 *
 */
class ApplicationController extends AbstractActionController {
	private $contactsTable;
	private $relationshipsTable;
	private $searchIndexTable;

	// Returns connection to Contacts-Table
	protected function getContactsTable() {
		if(!$this->contactsTable) {
			$sm = $this->getServiceLocator();
			$this->contactsTable = $sm->get('Application\Model\ContactsTable');
		}
		return $this->contactsTable;
	}

	// Returns connection to Relationships-Table
	protected function getRelationshipsTable() {
		if(!$this->relationshipsTable) {
			$sm = $this->getServiceLocator();
			$this->relationshipsTable = $sm->get('Application\Model\RelationshipsTable');
		}
		return $this->relationshipsTable;
	}
	
	// Returns connection to Search Index-Table
	protected function getSearchIndexTable() {
		if(!$this->searchIndexTable) {
			$sm = $this->getServiceLocator();
			$this->searchIndexTable = $sm->get('Application\Model\IndexTable');
		}
		return $this->searchIndexTable;
	}
}

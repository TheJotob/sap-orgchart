<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Application\Model\Api\Nodes;
use Application\Model\Api\Relationships;

/**
 * Controller to handle API Requests in a ActionController manner
 * @author Joachim Eckerlin <joachim.eckerlin@gmail.com>
 *
 */
class ApiController extends ApplicationController {

	// Displays search results as JSON
	public function searchAction() {
		header('Content-Type: application/json');
		$json = json_encode($this->searchFor($this->params()->fromPost('query')));
		$this->getResponse()
			->setContent($json);
		return $this->getResponse();
	}
	
	// Displays children of <URL Parameter node_id> over <URL Parameter level>
	public function childrenAction() {
		header('Content-Type: application/json; charset=utf-8');
		$node_id = array($this->params('node_id'));
		$level = $this->params('level');
		
		$nodes = new Nodes($this->getContactsTable());
		$relationships = new Relationships($this->getRelationshipsTable());

		$children_array = array(
				'contacts' => $nodes->getChildren($node_id, $level),
				'relationships' => $relationships->getRelationships($nodes->getNodeIds())
		);
		$this->getResponse()->setContent(json_encode($children_array));
		return $this->getResponse();
	}
	
	// Displays parents and their children of <URL Parameter node_id> over <URL Parameter level>
	public function parentsAction() {
		header('Content-Type: application/json; charset=utf-8');
		$node_id = array($this->params('node_id'));
		$level = $this->params('level');

		$nodes = new Nodes($this->getContactsTable());
		$relationships = new Relationships($this->getRelationshipsTable());

		$parents_array = array(
				'contacts' => $nodes->getParents($node_id, $level),
				'relationships' => $relationships->getRelationships($nodes->getNodeIds())
		);
		$this->getResponse()->setContent(json_encode($parents_array));
		return $this->getResponse();
	}
	
	// Combines parentsAction and childrenAction
	public function startAction() {
		header('Content-Type: application/json; charset=utf-8');
		$node_id = array($this->params('node_id'));
		$level = $this->params('level');

		$nodes = new Nodes($this->getContactsTable());
		$node_array = array_merge(	$nodes->getParents($node_id, $level),
									$nodes->getChildren($node_id, $level));
		
		$relationships = new Relationships($this->getRelationshipsTable());

		$start_array = array(
				'contacts' => $node_array,
				'relationships' => $relationships->getRelationships($nodes->getNodeIds())
		);
		$this->getResponse()->setContent(json_encode($start_array));
		return $this->getResponse();
	}

	// Returns search Results as Array
	private function searchFor($query) {
		$objects = array();
		$results = $this->getSearchIndexTable()
			->search($query);
		foreach ( $results as $result )
			$objects [] = $result;
		return $objects;
	}
}

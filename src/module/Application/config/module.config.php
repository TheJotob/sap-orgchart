<?php
return array(
		'router' => array(
				'routes' => array(
						'home' => array(
								'type' => 'Literal',
								'options' => array(
										'route' => '/',
										'defaults' => array(
												'controller' => 'Index',
												'action' => 'index')),
								'may_terminate' => true),
						'import' => array(
								'type' => 'Literal',
								'options' => array(
										'route' => '/import/json',
										'defaults' => array(
												'controller' => 'Index',
												'action' => 'import'))),
						'api' => array(
								'type' => 'Literal',
								'options' => array(
										'route' => '/api',
										'defaults' => array(
												'controller' => 'Api',
												'action' => 'index')),
								'may_terminate' => false,
								'child_routes' => array(
										'search' => array(
												'type' => 'Literal',
												'options' => array(
														'route' => '/search',
														'defaults' => array(
																'action' => 'search'))),
										'children' => array(
												'type' => 'Segment',
												'options' => array(
														'route' => '/children/:node_id[/:level]',
														'constraints'=> array(
																'node_id' => '[0-9]*',
																'level' => '[0-9]*'),
														'defaults' => array(
																'action' => 'children',
																'level' => '1'))),
										'parents' => array(
												'type' => 'Segment',
												'options' => array(
														'route' => '/parents/:node_id[/:level]',
														'constraints'=> array(
																'node_id' => '[0-9]*',
																'level' => '[0-9]*'),
														'defaults' => array(
																'action' => 'parents',
																'level' => '1'))),
										'start' => array(
												'type' => 'Segment',
												'options' => array(
														'route' => '/start/:node_id[/:level]',
														'constraints'=> array(
																'node_id' => '[0-9]*',
																'level' => '[0-9]*'),
														'defaults' => array(
																'action' => 'start',
																'level' => '1'))))))),
		'controllers' => array(
				'invokables' => array(
						'Index' => 'Application\Controller\IndexController',
						'Api' => 'Application\Controller\ApiController')),
		'view_manager' => array(
				'display_not_found_reason' => true,
				'display_exceptions' => true,
				'doctype' => 'HTML5',
				'not_found_template' => 'error/404',
				'exception_template' => 'error/index',
				'template_map' => array(
						'layout/layout' => __DIR__ . '/../view/layout/layout.phtml',
						'application/index/index' => __DIR__ . '/../view/index/index.phtml',
						'application/index/import' => __DIR__ . '/../view/index/import.phtml',
						'application/index/print' => __DIR__ . '/../view/index/print.phtml',
						'error/404' => __DIR__ . '/../view/error/404.phtml',
						'error' => __DIR__ . '/../view/error/index.phtml'),
				'template_path_stack' => array(
						__DIR__ . '/../view')));

<?php
namespace Application;

use Zend\Mvc\MvcEvent;
use Zend\Mvc\ModuleRouteListener;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Application\Model\Contact;
use Application\Model\ContactsTable;
use Application\Model\Relationship;
use Application\Model\RelationshipsTable;
use Application\Model\SearchAttribute;
use Application\Model\IndexTable;
use Zend\Http\Header\Connection;

class Module {

	public function onBootstrap(MvcEvent $e) {
		$eventManager = $e->getApplication()
			->getEventManager();
		$moduleRouteListener = new ModuleRouteListener();
		$moduleRouteListener->attach($eventManager);
	}

	public function getConfig() {
		return include __DIR__ . '/config/module.config.php';
	}

	public function getAutoloaderConfig() {
		return array(
				'Zend\Loader\StandardAutoloader' => array(
						'namespaces' => array(
								__NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__)));
	}

	public function getServiceConfig() {
		return array(
				'factories' => array(
						'Application\Model\ContactsTable' => function ($sm) {
							$tableGateway = $sm->get('ContactsTableGateway');
							$table = new ContactsTable($tableGateway);
							return $table;
						},
						'ContactsTableGateway' => function ($sm) {
							$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
							$resultSetPrototype = new ResultSet();
							$resultSetPrototype->setArrayObjectPrototype(new Contact());
							return new TableGateway('contacts', $dbAdapter, null, $resultSetPrototype);
						},
						'Application\Model\RelationshipsTable' => function ($sm) {
							$tableGateway = $sm->get('RelationshipsTableGateway');
							$table = new RelationshipsTable($tableGateway);
							return $table;
						},
						'RelationshipsTableGateway' => function ($sm) {
							$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
							$resultSetPrototype = new ResultSet();
							$resultSetPrototype->setArrayObjectPrototype(new Relationship());
							return new TableGateway('connections', $dbAdapter, null, $resultSetPrototype);
						},
						'Application\Model\IndexTable' => function ($sm) {
							$tableGateway = $sm->get('IndexTableGateway');
							$table = new IndexTable($tableGateway);
							return $table;
						},
						'IndexTableGateway' => function ($sm) {
							$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
							$resultSetPrototype = new ResultSet();
							$resultSetPrototype->setArrayObjectPrototype(new SearchAttribute());
							return new TableGateway('search_index', $dbAdapter, null, $resultSetPrototype);
						}));
	}
}

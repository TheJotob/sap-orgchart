$(document).ready(function() {
	//give the reset button the function to reset the active path 
	$("#button_reset").click(function(){
				resetActivePath();
	});		
//Zoom  Buttons
	//gives button the function to zoom in 
	$("#button_plus").click(function(){
				zoomButtonPlus();
	});		
		
	//gives button the function to zoom out 
	$("#button_minus").click(function(){
				zoomButtonMinus();
	});		
	
	//Set new Settings in settings modal (loadlevel, show informal relation, show axis name, foldable graph) 
	
		$('#button_submit').click(function (){
			 //gives the variable removeChildren the right boolean of the checkbox
			 removeChildren = $('#checkbox_fold').is(':checked');
			//checking if the new value for loadlevel is correct, and will be saved if its coorect
			if( $('#input_levels').val()>5 || $('#input_levels').val()<1 ){
				alert("Anzahl der Ebenen die beim Klick geladen werden sollen ist zu groß oder zu klein und wurde deswegen nicht geändert! (min:1, max:5)");
				$("#input_levels").val(loadlevel);
			}
			else{
				loadlevel = $('#input_levels').val();
			}
			//will change the view of the graph by adding or removing the informal edges
			if ($('#checkbox_informalrelation').is(':checked')) {
			    showInformalRelation = true;
			    drawInformalEdges();
			 }
			 else{
			 	showInformalRelation = false;
			 	//alert(showInformalRelation);
			 	removeInformalEdges();
			 }
			 //will change the view of the graph by adding or removing the axis label
			 if ($('#checkbox_axis').is(':checked')) {
			    showRelationshipLabel = true;
			    //alert(showRelationshipLabel);
			    toggleLabels();
			 }
			 else{
			 	showRelationshipLabel = false;
				//alert(showRelationshipLabel);
			 	toggleLabels();
			 }    

			$("#settingsModal").hide();
			
		});
		
});	

	/*the reallyStartFunction is the first function that is used when the page is loaded to send a request to get the json for the  first levels of the company */
		function reallyStartFunction(ID){	
				jQuery.ajax({
					  url:         basePath+"/api/start/"+parseInt(ID)+"/"+parseInt(loadlevel),
					  dataType:    "json",
					  mimeType:		"textPlain",
					  success:     function(data){
										resetGraph();
			
						                jQuery.each(data.contacts, function(i){
										newNode(data.contacts[i]);
										});
										jQuery.each(data.relationships, function(i){
											newEdge(data.relationships[i]);
										});
										createGraph();	
										centerNode(ID)
										
		               },
					  error:       function(e){alert('oops: ' + e);}
					});
		}

		/* the startfunction is used while the search is in progress, to make a request and giving back the first nodes that should be displayed*/ 
	 	function startfunction(ID){	
				jQuery.ajax({
					  url:         basePath+"/api/start/"+parseInt(ID)+"/"+parseInt(loadlevel),
					  dataType:    "json",
					  mimeType:		"textPlain",
					  success:     function(data){
										resetGraph();
			
						                jQuery.each(data.contacts, function(i){
										newNode(data.contacts[i]);
										});
										jQuery.each(data.relationships, function(i){
											newEdge(data.relationships[i]);
										});
										createGraph();	
										centerNode(ID)
										changeNodeToActive(ID);
										
		               },
					  error:       function(e){alert('oops: ' + e);}
					});
		}

		
		function loadNewLevel(ID){
			if(!hasFormalChild(ID)){
					jQuery.ajax({	
					  url:         basePath+"/api/children/"+parseInt(ID)+"/"+1,
					  dataType:    "json",
					  mimeType:		"textPlain",
							  success:     function(data){			                jQuery.each(data.contacts, function(i){
			                	newNode(data.contacts[i]);
							});
							jQuery.each(data.relationships, function(i){
								newEdge(data.relationships[i]);
							});
							createGraph();	
							centerNode(ID);
		               },
						//this opens an alert-window, if reading of json is not successful
					  error:       function(e){alert('oops: ' + e);}
					});		
			}
			if(!hasFormalParent(ID)){
				if(ID == 1){
					return;
				}
					jQuery.ajax({	
					  url:         basePath+"/api/parents/"+parseInt(ID)+"/"+1,
					  dataType:    "json",
					  mimeType:		"textPlain",
					  success:     function(data){
			                jQuery.each(data.contacts, function(i){
			                	newNode(data.contacts[i]);
												
							});
							jQuery.each(data.relationships, function(i){
								newEdge(data.relationships[i]);
							});
											createGraph();
											centerNode(ID);
		               },
						//this opens an alert-window, if reading of json is not successful
					  error:       function(e){alert('oops: ' + e);}
					});				
			}
		}
	


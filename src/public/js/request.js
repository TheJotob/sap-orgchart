$(document).ready(function() {
	//using reset function 
	$("#button_reset").click(function(){
				resetActivePath();
	});		
	//Zoom  Buttons
	$("#button_plus").click(function(){
				zoomButtonPlus();
	});		
	$("#button_minus").click(function(){
				zoomButtonMinus();
	});		
	

	//Set new Settings (loadlevel, show informal relation, show axis name
		$('#button_submit').click(function (){
			if( $('#input_levels').val()>5 || $('#input_levels').val()<1 ){
				alert("Anzahl der Ebenen die beim Klick geladen werden sollen ist zu groß oder zu klein und wurde deswegen nicht geändert! (min:1, max:5)");
			}
			else{
				loadlevel = $('#input_levels').val();
			}
			if (document.getElementById('checkbox_informalrelation').checked  == true) {
			    showInformalRelation = true;
			    //alert(showInformalRelation);
			    drawInformalEdges();
			 }
			 else{
			 	showInformalRelation = false;
			 	//alert(showInformalRelation);
			 	removeInformalEdges();
			 	
			 }
			 if (document.getElementById('checkbox_axis').checked  == true) {
			    showRelationshipLabel = true;
			    //alert(showRelationshipLabel);
			    toggleLabels();
			 }
			 else{
			 	showRelationshipLabel = false;
			 	//alert(showRelationshipLabel);
			 	toggleLabels();
			 }    
			$("#settingsModal").hide();
			
		});
 
/*print function*/	

		$('#button_print').click(function (){
			$(button_activePath).show();
			$(button_submitPrint).show();
			$(button_stopPrint).show();
			heigthsvg = $("#svg").height();
			widthsvg = $("#svg").width();
			d3.select("svg").append("rect")
							.attr("id","printRect")
							.attr("x",0)
							.attr("y",0)
							.attr("width",900 )
							.attr("height",heigthsvg)
							.attr("fill", "none")
							.attr("stroke-width",5)
							.attr("stroke", "black")
							.attr("visibility", "show")
			$("#button_submitPrint").click(function(){
				if(printActivePath==false){
					$("#printRect").remove();
					$(button_submitPrint).hide();
					$(button_activePath).hide();
					$(button_stopPrint).hide();
					window.print();
				}
				else{
					$("#printRect").remove();
					$(button_submitPrint).hide();
					$(button_activePath).hide();
					$(button_stopPrint).hide();
					window.print();
					$("printableGraph").attr("opacity",0);
					$("g").attr("opacity",1);		
				}
			});			
			$("#button_stopPrint").click(function(){
				if(printActivePath== false){
					$("#printRect").remove();
					$(button_activePath).hide();
					$(button_submitPrint).hide();
					$(button_stopPrint).hide();
				}
				else{
					$("#printRect").remove();
					$(button_activePath).hide();
					$(button_submitPrint).hide();
					$(button_stopPrint).hide();
					$("printableGraph").attr("opacity",0);
					$("g").attr("opacity",1);		
				}
				});		
			$("#button_activePath").click(function(){
				printActivePath= true;
				//$("g").attr("opacity",0);
				drawPrintableGraph();
			});	
		 });
		
	/*
		$('#button_print').click(function (){
			$('#svg').printArea({printMode:'popup'});
		});
		*/
/*			
		$('#button_print').click(function (){
			 function nextGraph(){

		// Run the renderer. This is what draws the final graph.
			d3.select("svg g").call(render, g);

		//click- and mouseover-events are added here	
			addNodeEvents();
			}
		 });
		 

		
 */
});	

	
	
		function startfunction(ID){	
				jQuery.ajax({
					
					  url:         basePath+"/api/start/"+parseInt(ID)+"/"+parseInt(loadlevel),
					  dataType:    "json",
					  mimeType:		"textPlain",
					  success:     function(data){
										resetGraph();
			
						                jQuery.each(data.contacts, function(i){
										newNode(data.contacts[i]);
										});
										jQuery.each(data.relationships, function(i){
											newEdge(data.relationships[i]);
										});
										createGraph();	
										changeNodeToActive(ID);
										centerNode(ID);
		               },
						//this opens an alert-window, if reading of json is not successful
					  error:       function(e){alert('oops: ' + e);}
					});
		}
		
		
		
		function loadNewLevel(ID){
			if(!hasFormalChild(ID)){
					jQuery.ajax({	
					  url:         basePath+"/api/children/"+parseInt(ID)+"/"+1,
					  dataType:    "json",
					  mimeType:		"textPlain",
							  success:     function(data){			                jQuery.each(data.contacts, function(i){
			                	newNode(data.contacts[i]);
							});
							jQuery.each(data.relationships, function(i){
								newEdge(data.relationships[i]);
							});
							createGraph();	
							centerNode(ID);
		               },
						//this opens an alert-window, if reading of json is not successful
					  error:       function(e){alert('oops: ' + e);}
					});		
			}
			if(!hasFormalParent(ID)){
				if(ID == 1){
					return;
				}
					jQuery.ajax({	
					  url:         basePath+"/api/parents/"+parseInt(ID)+"/"+1,
					  dataType:    "json",
					  mimeType:		"textPlain",
					  success:     function(data){
			                jQuery.each(data.contacts, function(i){
			                	newNode(data.contacts[i]);
												
							});
							jQuery.each(data.relationships, function(i){
								newEdge(data.relationships[i]);
							});
											createGraph();
											centerNode(ID);
		               },
						//this opens an alert-window, if reading of json is not successful
					  error:       function(e){alert('oops: ' + e);}
					});				
			}
		}
	


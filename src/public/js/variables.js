
//empty graph
var g = new dagreD3.graphlib.Graph({directed:true, multigraph:true, compound:true})
	.setGraph({rankdir: "TB", nodesep: 200, edgesep: 50, ranksep: 100})
	.setDefaultEdgeLabel(function() { return {}; });
		
//for active path and creating a printable graph
var activeEdges=[];
var activeNodes=[];

//contains all infos needed to display informal relationships
var informalEdges=[];

//array for all details shown in tooltip
var details = [];

//can be changed when changing the configurations can be true or false
var showRelationshipLabel = false;

//for function removing child-nodes when node is deactivated
var removeChildren = true;

//showInformalRelation can be true or false
var showInformalRelation=false;

var init = true;

var inner; // for center Node and ZoomButtons
var zoom;

//renderer
var render = new dagreD3.render();

//variables for saving levels that will be loaded on a clickevent 
var loadedparents;
var loadedchildren;

//Levels that should be shown/loaded after search 
var loadlevel=2;




//saves contact details in details[nodeData.Contact_ID]
function contactDetails(nodeData){
	if(typeof details[nodeData.Contact_ID] == 'undefined'){
		initializeNodeDetails(nodeData.Contact_ID);
	}
	details[nodeData.Contact_ID].notes = nodeData.memos;
	details[nodeData.Contact_ID].attachments = nodeData.attachments;		
	details[nodeData.Contact_ID].mails = nodeData["e-Mail"];
	details[nodeData.Contact_ID].numbers = nodeData.nummer;
	details[nodeData.Contact_ID].addresses = nodeData.addresses;
}

//initializes details[nodeId]
//so that we can work with empty arrays instead of undefined variables
function initializeNodeDetails(nodeId){
	details[nodeId] = [];
	details[nodeId].notes = [];
	details[nodeId].attachments = [];
	details[nodeId].mails = [];
	details[nodeId].numbers = [];
	details[nodeId].addresses = [];
}

//adds contact information to details[relData.Partner2_ID]
function relationshipDetails(relData){
	var id = relData.Partner2_ID;
	if(relData.notiz){
		details[id].notes = details[id].notes.concat(relData.notiz);
	}
	if(relData.anlage){
		details[id].attachments = details[id].attachments.concat(relData.anlage);
	}
	if(relData["e-Mail"]){
		details[id].mails = details[id].mails.concat(relData["e-Mail"]);
	}
	if(relData.nummer){
		details[id].numbers = details[id].numbers.concat(relData.nummer);
	}
	if(relData.adresse){
		details[id].addresses = details[id].addresses.concat(relData.adresse);
	}
}


//returns true if node has at least one child
//(only formal relationships)
function hasFormalChild(nodeId){
	var edges = g.outEdges(nodeId);
	if(edges == undefined)
		return false;
	
	for(var i=0; i<edges.length; i++){
		if(isFormalEdge(edges[i])){
			return true;
		}
	}
	return false;
}

//returns true if node has at least one parent
//(only formal relationships)
function hasFormalParent(nodeId){
	var edges = g.inEdges(nodeId);
	if(edges == undefined)
		return false;
	
	for(var i=0; i<edges.length; i++){
		if(isFormalEdge(edges[i])){
			return true;
		}
	}
	return false;	
}

//returns true if edgeObj is a formal edge/relationship
function isFormalEdge(edgeObj){
	var edgeType = g.edge(edgeObj).type;
	if(edgeType=="100" || edgeType=="101" ||edgeType=="110" ||edgeType=="111"){
		return true;
	}
	return false;
}

//checking if active node already has an active child
function hasActiveDownEdge(nodeId){
	for(var i=0; i<activeEdges.length; i++){
		if(activeEdges[i].v == nodeId){
			return true;
		}
	}
	return false;
}

//checking if active node already has active parent
function hasActiveUpEdge(nodeId){
	for(var i=0; i<activeEdges.length; i++){
		if(activeEdges[i].w == nodeId){
			return true;
		}
	}
	return false;
}


//returns true if node can be connected to active path
function connectedToActivePath(nodeId){
	for(var i=0; i<activeNodes.length; i++){
		if(hasFormalEdge(nodeId, activeNodes[i])){
			return true;
		}
	}
	return false;
}


//to make sure that the active path only contains nodes that are
//connected through a formal relationship
//and that there is only one straight line
//no to children of one active node can be active at the same time
function hasFormalEdge(nodeId, activeNodeId){
	var upEdges = g.inEdges(nodeId, activeNodeId);
	for(var i=0; i<upEdges.length; i++){
		if(!hasActiveDownEdge(activeNodeId) && isFormalEdge(upEdges[i])){
			changeEdgeToActive(upEdges[i]);
			return true;
		}
	}
	var downEdges = g.outEdges(nodeId, activeNodeId);
	for(var i=0; i<downEdges.length; i++){
		if(!hasActiveUpEdge(activeNodeId) && isFormalEdge(downEdges[i])){
			changeEdgeToActive(downEdges[i]);
			return true;
		}
	}
	return false;
}


// string-variable 'all' will be displayed in tooltip
var styleTooltip = function(nodeId) {
		var all = "";
		var address = getAdress(nodeId);
		var numbers = getCommunicationData(nodeId);
		var notes = getNotes(nodeId);
		var attachments = getAttachments(nodeId);
		all = all.concat(address);
		all = all.concat(numbers);
		all = all.concat(notes);
		all = all.concat(attachments);
		return all;
	};

	

//returns address of node with id nodeId in different format
function getAdress(nodeId){
	var text = "<p class='name'>" + "Adressen: " + "</p>";
	for(var i=0; i<details[nodeId].addresses.length; i++){
		var address = "<p class='description'> - </p>"
					+"<p class='description'>"+details[nodeId].addresses[i].Street+" "+details[nodeId].addresses[i]["House No."]+ "</p>"
					+"<p class='description'>"+details[nodeId].addresses[i]["Post Code"]+ " "+details[nodeId].addresses[i].City +"</p>"
					+"<p class='description'>"+details[nodeId].addresses[i].Country+ "<br>" +"</p>";
		text = text.concat(address);
	}
	return text;
}

//returns communication data (phone numbers, mail addresses) of node with id nodeId in different format
function getCommunicationData(nodeId){
	var text = "<p class='name'>" + "Nummern: " + "</p>";
	for(var i=0; i<details[nodeId].numbers.length; i++){
		var number = "<p class='description'>"+details[nodeId].numbers[i].Number_Type+": <br>"+details[nodeId].numbers[i].Countryiso+"-"+details[nodeId].numbers[i].Number+" "+details[nodeId].numbers[i].Extension +"</p>";
			text = text.concat(number);
	}
	text = text.concat("<p class='name'>" + "E-Mail: " + "</p>");
	for(var i=0; i<details[nodeId].mails.length; i++){
		var mail = "<p class='description'>"+details[nodeId].mails[i]["E-Mail"]+"</p>";
		text = text.concat(mail);
	}
	return text;
}

//returns notes of node with id nodeId in different format
function getNotes(nodeId){
	var text = "<p class='name'>" + "Notizen: " + "</p>";
	for(var i=0; i<details[nodeId].notes.length; i++){
		var note = "<p class='description'> - "+details[nodeId].notes[i].Notiz +"</p>";
		text = text.concat(note);
	}
	return text;
}

//returns attachments of node with id nodeId in different format
function getAttachments(nodeId){
	var text = "<p class='name'>" + "Anlagen: " + "</p>";
	for(var i=0; i<details[nodeId].attachments.length; i++){
		var attachments = "<a class='description', href ="+details[nodeId].attachments[i]["Attachement-URL"]+" >"+ details[nodeId].attachments[i]["Attachement-Name"]  +"<br></a>";
		text = text.concat(attachments);
	}
	return text;
}

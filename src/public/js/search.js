/* Search Client Side */
var results;
var lastRequest = 1;

$(document).ready(function() {
	$('#search_field').keyup(function() {
		searchFor($('#search_field').val());
	});

	$('.search').focusin(function() {
		$('#search_result').fadeIn();
	});

	$('.search').focusout(function() {
		$('#search_result').fadeOut();
	});
});

function setResultHandler() {
	$('.result').click(function() {
		var contact = $(this).data('contact');
		startfunction(contact.id);
	})
}

function searchFor(query) {
	if(query.length >= 2) {
		var requestNumber = lastRequest + 1;
		lastRequest = requestNumber;

		$.ajax({
			url : basePath + '/api/search',
			data : {
				'query' : query
			},
			dataType : "json",
			mimeType : "textPlain",
			method : 'POST',
			success : function(data) {
				results = data;
				if(requestNumber = lastRequest)
					showResults(data, query);
			},
			// this opens an alert-window, if reading of json is not successful
			error : function(e) {
				console.log(e)
			}
		});
	}
}

function showResults(results, query) {
	$('#resultList').empty();
	for (var i = 0; i < results.length; i++) {
		$('#resultList').append(getResultHtml(results[i], query));
		$('#result' + results[i].id).data("contact", results[i]);
	}

	setResultHandler();
}

function getResultHtml(result, query) {
	var name = result.name.replace(query, '<strong>' + query + '</strong>');
	var value = result.value.replace(query, '<strong>' + query + '</strong>');
	var html = '<li class="list-group-item result" id="result' + result.id
			+ '">' + '<div class="result-name">' + name + '</div>'
			+ '<div class="result-hit">' + result.field + ': ' + value
			+ '</div>' + '</li>';
	return html;
}
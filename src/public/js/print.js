$(document).ready(function() {

//function for printing the graph
/* Draws on click(button_print) a rect in the svg that shows the area that will be shown in the print dialog.
Two buttons will be open, one for stop the printing function and remove the rect and the buttons and the second one 
for open the print diaolog of the browser! On click to one of the bottons the rect and the buttons will be removed.*/

		$('#button_print').click(function (){
			$(button_submitPrint).show();
			$(button_stopPrint).show();
			heigthsvg = $("#svg").height();
			widthsvg = $("#svg").width();
			d3.select("svg").append("rect")
							.attr("id","printRect")
							.attr("x",0)
							.attr("y",0)
							.attr("width",900 )
							.attr("height",heigthsvg)
							.attr("fill", "none")
							.attr("stroke-width",5)
							.attr("stroke", "black")
							.attr("visibility", "show")
			$("#button_submitPrint").click(function(){
					$("#printRect").remove();
					$(button_submitPrint).hide();
					$(button_stopPrint).hide();
					window.print();
				
			});			
			$("#button_stopPrint").click(function(){
					$("#printRect").remove();
					$(button_submitPrint).hide();
					$(button_stopPrint).hide();
				});		

		 });
});	
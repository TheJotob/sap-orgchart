
//removes all elements form informalEdges form graph
function removeInformalEdges(){
	for(var i=0; i<informalEdges.length; i++){
		g.removeEdge(informalEdges[i].Partner1_ID, informalEdges[i].Partner2_ID);
	}
	//redraw the graph (layout changed)
	d3.select("svg g").call(render, g);	
}

//adds all edges from informalEdges to graph
function drawInformalEdges(){
	for(var i=0; i<informalEdges.length; i++){
		if(g.hasNode(informalEdges[i].Partner1_ID) && g.hasNode(informalEdges[i].Partner2_ID)){
			//only show labels if showRelationshipLabel is currently true
			//if showRelationshipLabel has been changed while informal edges were not in the graph,
			// the changed settings would get lost here
			var showLabel = "font-size: 14px; fill: none;";
			if(showRelationshipLabel){
				showLabel="font-size: 14px; fill: black;";
			}
			g.setEdge(informalEdges[i].Partner1_ID, informalEdges[i].Partner2_ID,
			{
				id: informalEdges[i].id,
				label: informalEdges[i].label,
				type: informalEdges[i].type,
				weight: 1,
				labelStyle: showLabel,
				arrowhead: informalEdges[i].arrowhead,
				arrowheadStyle: informalEdges[i].arrowheadStyle,
				class: "type-"+informalEdges[i].type,
			}
			);
		}
	}
	//redraw graph (layout has changed)
	d3.select("svg g").call(render, g);		
}


//shows or hides labels
function toggleLabels(){
	if(showRelationshipLabel){
		d3.selectAll(".edgeLabel").select(".label").select("text").style("fill", "black");
	}
	else{
		d3.selectAll(".edgeLabel").select(".label").select("text").style("fill", "none");	
	}
	
	var showLabel = "font-size: 14px; fill: none;";
	if(showRelationshipLabel){
		showLabel="font-size: 14px; fill: black;";
	}

	var edges = g.edges();
	for(var i=0; i<edges.length; i++){
		 g.setEdge(edges[i].v,edges[i].w,
		{
			id: g.edge(edges[i]).id,
			label: g.edge(edges[i]).label,
			type: g.edge(edges[i]).type,
			weight: g.edge(edges[i]).weight,
			labelStyle:showLabel,
			arrowhead: g.edge(edges[i]).arrowhead,
			arrowheadStyle: g.edge(edges[i]).arrowheadStyle,
			class: g.edge(edges[i]).class
		});
	}
}
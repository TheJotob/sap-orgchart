
//creates new nodes and adds them to graph
function newNode(node){
	//if node with id node.Contact_ID already exists, do nothing
	if(g.hasNode(node.Contact_ID)){
		return;
	}
	
	//if node.Type=="Organisation": label of node contains different attributes
	if(node.Type=="Organisation"){
		g.setNode(node.Contact_ID,{
			id: "contact"+node.Contact_ID,
			title: "",
			label: node.Name1 + "\n" + node.Name2,
			type:  node.Type,
			class: "type-"+node.Type
		});
	}
	else{
		g.setNode(node.Contact_ID,{
			id: "contact"+node.Contact_ID,
			title:"",
			label: node.Title_Key + " " + node.Academic_Title + "\n" + node.Firstname + " " + node.Lastname+"\nAbteilung: "+node.Department_Name+"\nFunktion: "+node.Function_Name,
			type:  node.Type,
			class: "type-"+node.Type
		});
	}
	
	//add context information to array details
	contactDetails(node);
}


//creates new edges and adds them to graph
function newEdge(edge){
	//if edge already exists or if start- or endnode do not exist in graph: do nothing
	if(!g.hasNode(edge.Partner1_ID) || !g.hasNode(edge.Partner2_ID) || g.hasEdge(edge.Partner1_ID, edge.Partner2_ID)){
		return;
	}
	
//weight influences the edges: heavier edges should be made shorter and straighter
//can be changed, erased or adapted if needed
	var gewicht = 1;
	    if(edge.Formal=="1"){
			gewicht += 100;
		}	
		
//edge-labels are invisible, unless "showRelationshipLabel" is true
	var showLabel = "font-size: 14px; fill: none;";
	if(showRelationshipLabel){
		showLabel="font-size: 14px; fill: black;";
	}
	
	//arrow=="normal" for directed edges
	//arrow=="undirected" for undirected edges
	var arrow;
	//colour of arrow should be the same as the colour of the edge
	var arrowStyle;
	if(edge.Directed == 1){
		arrow="normal";
		if(edge.Color=="00"){
			arrowStyle="stroke:blue; fill:blue; stroke-width:1px;";
		}
		else if(edge.Color=="01"){
			arrowStyle="stroke:green; fill:green; stroke-width:1px;";
		}
		else if(edge.Color=="10"){
			arrowStyle="stroke:#DAA520; fill:#DAA520; stroke-width:1px;";
		}
		else if(edge.Color=="11"){
			arrowStyle="stroke:red; fill:red; stroke-width:1px;";
		}
	}
	else{
		arrow="undirected";
		arrowStyle="stroke-width: 1px;";
	}

//does not add informal relationships to graph if showInformalRelation==false	
//adds an edge-object to array informalEdges
	if(edge.Formal.toString()=="0"){
		var informalEdge = {
			'Partner1_ID': edge.Partner1_ID, 
			'Partner2_ID': edge.Partner2_ID, 
			'id' :"relationship"+edge.Partner1_ID+"to"+edge.Partner2_ID+"isa"+edge.Type,
			'label': edge.Comment,
			'type': edge.Formal.toString().concat(edge.Color),
			'arrowhead': arrow,
			'arrowheadStyle': arrowStyle,
		}
		informalEdges.push(informalEdge);
		if(!showInformalRelation){
			return;
		}	
	}
	
	g.setEdge(edge.Partner1_ID, edge.Partner2_ID,
		{
			id: "relationship"+edge.Partner1_ID+"to"+edge.Partner2_ID+"isa"+edge.Type,
			label: edge.Comment,
			type: edge.Formal.toString().concat(edge.Color),
			weight: gewicht,
			labelStyle: showLabel,
			arrowhead: arrow,
			arrowheadStyle: arrowStyle,
			class: "type-"+edge.Formal.toString().concat(edge.Color),
		}
	);
	
	//adds context information to variable details
	relationshipDetails(edge);
}


//function adds click- and mouseover-event to each node in g
function addNodeEvents(){
	d3.select("svg").selectAll("g.node")
		.on("click",	function(d) {
			//trying to hide any tooltips
			$(this).tipsy('hide');
			
			activatePath(d);
			})	
		.on("mouseover", function(d){	
			$(this).attr("original-title", function() { return styleTooltip(d); });
		})
		
		.each(function(v) { $(this).tipsy({ gravity: "w", opacity: 1, html: true }); });	  
}


//in case we need to remove the events for each node in g
function removeNodeEvents(){
	d3.select("svg").selectAll("g.node")
		.on("click",null)
		.each(function(v) { $(this).tipsy('disable'); });
}


//draws the actual graph
function createGraph(){	
	if(init){
		firstGraph();
	}
	else{
		nextGraph();
	}

}

//graph is drawn for the first time
//sets up zoom behaviour
function firstGraph(){
	
	init = false;
	
	// Set up an SVG group so that we can translate the final graph.
	var svg = d3.select("svg");
	inner = svg.append("g");
		
//set up zoom behaviour
	zoom = d3.behavior.zoom().on("zoom", function() {
		inner.attr("transform", "translate(" + d3.event.translate + ")" +
									"scale(" + d3.event.scale + ")");
	  });
	svg.call(zoom);	
        svg.on("dblclick.zoom", null);
	
// Run the renderer. This is what draws the final graph.
	d3.select("svg g").call(render, g);

//click- and mouseover-events are added here	
	addNodeEvents();

// Zoom and scale to fit
 /*   var zoomScale = zoom.scale();
    var graphWidth = g.graph().width + 80;
    var graphHeight = g.graph().height + 40;
    var width = parseInt(svg.style("width").replace(/px/, ""));
    var height = parseInt(svg.style("height").replace(/px/, ""));
    zoomScale = Math.min(width / graphWidth, height / graphHeight);
    var translate = [(width/2) - ((graphWidth*zoomScale)/2), (height/2) - ((graphHeight*zoomScale)/2)];
    zoom.translate(translate);
    zoom.scale(zoomScale);
	zoom.event(svg);
*/
}


function nextGraph(){
	
	d3.select("svg").selectAll("g.node").each(function(){
				$(this).tipsy('hide');
			});
	
// Run the renderer. This is what draws the final graph.
	d3.select("svg g").call(render, g);

//click- and mouseover-events are added here	
	addNodeEvents();
}


//resets graph
function resetGraph(){
	resetActivePath();
	informalEdges=[];
//rankdir: T=Top, B=Bottom
//nodesep: horizontal distance between nodes
//edgesep: horizontal distance between edges
//ranksep: vertical distance between nodes
	g = new dagreD3.graphlib.Graph({directed:true, multigraph:true, compound:true})
		.setGraph({rankdir: "TB", nodesep: 200, edgesep: 50, ranksep: 100})
		.setDefaultEdgeLabel(function() { return {}; });
	d3.select("svg g").call(render, g);
}
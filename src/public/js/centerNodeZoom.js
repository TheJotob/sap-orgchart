//functions for centering Nodes and Zoom Buttons
function centerNode(nodeId)
{
	var element = document.getElementById("svg");                
	var svgWidth = parseFloat(window.getComputedStyle(element, null).width);   // Width  of the Svg-variable  e.g. (1800)
	var svgHeight = parseFloat(window.getComputedStyle(element, null).height); // Height of the Svg-variable  e.g. (700)
	var scaleRatio = zoom.scale();
//scaleRatio can be changed to adapt the zoom
//higher scaleRatio -> zoom closer to node
//lower scaleRatio -> zoom farer from node	
	scaleRatio = 0.65;                                              // Standard is ScaleRatio 1, because so you can read the node labels
	var xLog = scaleRatio * d3.transform(d3.select("#"+g.node(nodeId).id).attr("transform")).translate[0] - svgWidth / 2;     // Calculate xOffset
	var yLog = scaleRatio *d3.transform(d3.select("#"+g.node(nodeId).id).attr("transform")).translate[1] - svgHeight / 2;    // Calculate yOffset
	zoom.translate([-xLog,-yLog]).scale(scaleRatio);
	zoom.event(inner);                                                        // variant 1
	// zoom.event(inner.transition().duration(1200));                         // variant 2 with animation
	//inner.attr("transform", "translate("+ (-xLog) +","+ (-yLog)+") " +      // variant 3 without help of zoomlistener
		//	"scale("+scaleRatio+")");
}

function zoomButtonPlus()
{
	var scaleRatio = zoom.scale();
	scaleRatio = scaleRatio * 1.5;
	var element = document.getElementById("svg");                
	var svgWidth = parseFloat(window.getComputedStyle(element, null).width);   
	var svgHeight = parseFloat(window.getComputedStyle(element, null).height); 
	var graphWidth = scaleRatio *  (g.graph().width / 2) - svgWidth / 2;
	var graphHeight = scaleRatio * (g.graph().height / 2) - svgHeight / 2;
	zoom.translate([-graphWidth,-graphHeight]).scale(scaleRatio);
	zoom.event(inner);
}

function zoomButtonMinus()
{
	var scaleRatio = zoom.scale();
	scaleRatio = scaleRatio / 1.5;
	var element = document.getElementById("svg");                
	var svgWidth = parseFloat(window.getComputedStyle(element, null).width);   
	var svgHeight = parseFloat(window.getComputedStyle(element, null).height); 
	var graphWidth = scaleRatio *  (g.graph().width / 2) - svgWidth / 2;
	var graphHeight = scaleRatio * (g.graph().height / 2) - svgHeight / 2;
	zoom.translate([-graphWidth,-graphHeight]).scale(scaleRatio);
	zoom.event(inner);
}

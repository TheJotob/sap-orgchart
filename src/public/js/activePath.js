//functions for active path

//adds node to active path, if possible
//resets active node if node is already active and last or first node of active path
//otherwise does nothing
function activatePath(nodeId){
	if((activeNodes.length==0)||(activeNodes.indexOf(nodeId)==-1) && connectedToActivePath(nodeId)){		
		loadNewLevel(nodeId);        
		changeNodeToActive(nodeId);
		centerNode(nodeId);
	}
	else if(!(activeNodes.indexOf(nodeId)==-1)){
		resetActiveNode(nodeId);
		centerNode(nodeId);//added 
	}
}


//changes class of edge to active class
//adds edge to array activeEdges
function changeEdgeToActive(edgeObj){
	d3.select("#"+g.edge(edgeObj).id).classed(g.edge(edgeObj).class, false);
	d3.select("#"+g.edge(edgeObj).id).classed("type-"+g.edge(edgeObj).type+"-activated", true);
	 g.setEdge(edgeObj.v, edgeObj.w,
	{
		id:  g.edge(edgeObj).id,
		label: g.edge(edgeObj).label,
		type: g.edge(edgeObj).type,
		weight: g.edge(edgeObj).weight,
		labelStyle: g.edge(edgeObj).labelStyle,
		arrowhead: g.edge(edgeObj).arrowhead,
		arrowheadStyle: g.edge(edgeObj).arrowheadStyle,
		class: g.edge(edgeObj).class+"-activated"
	});
	activeEdges.push(edgeObj);
}

//changes class of nodes to active class
function changeNodeToActive(nodeId){	
	d3.select("#"+g.node(nodeId).id).classed(g.node(nodeId).class, false);
	d3.select("#"+g.node(nodeId).id).classed("type-"+g.node(nodeId).type+"-activated", true);
	g.setNode(nodeId,{
		title: g.node(nodeId).title,
		id: g.node(nodeId).id,
		label: g.node(nodeId).label,
		type: g.node(nodeId).type,
		class: "type-"+g.node(nodeId).type+"-activated"
	});
	activeNodes.push(nodeId);
}

//changes class of node to former, non-active class
function changeNodeToDeactive(nodeId){
	d3.select("#contact"+nodeId).classed(g.node(nodeId).class, false);
	d3.select("#contact"+nodeId).classed("type-"+g.node(nodeId).type, true);
	g.setNode(nodeId,{
		title: g.node(nodeId).title,
		id: g.node(nodeId).id,
		label: g.node(nodeId).label,
		type: g.node(nodeId).type,
		class: "type-"+g.node(nodeId).type
	});
//		centerNode(nodeId);//added 
}

//changes class of edge to former, non-active class
function changeEdgeToDeactive(edgeObj){
	d3.select("#"+g.edge(edgeObj).id).classed(g.edge(edgeObj).class, false);
	d3.select("#"+g.edge(edgeObj).id).classed("type-"+g.edge(edgeObj).type, true)
	g.setEdge(edgeObj.v, edgeObj.w,
	{
		id:  g.edge(edgeObj).id,
		label: g.edge(edgeObj).label,
		type: g.edge(edgeObj).type,
		weight: g.edge(edgeObj).weight,
		labelStyle: g.edge(edgeObj).labelStyle,
		arrowhead: g.edge(edgeObj).arrowhead,
		arrowheadStyle: g.edge(edgeObj).arrowheadStyle,
		class: "type-"+g.edge(edgeObj).type
	});

}

//reset single active node (and its active edges)
//if node is at top or bottom of active path
function resetActiveNode(nodeId){
	//if node has two active edges, it is in the middle of the active path
	//removing it would result in two separate paths
	if(!hasActiveDownEdge(nodeId) || !hasActiveUpEdge(nodeId)){
		var remove = !hasActiveDownEdge(nodeId);
		//reset class of active node (d3 and graph)

		//deactivate node
		changeNodeToDeactive(nodeId);

		//remove deactivated node from array activeNodes
	for(var i=0; i<activeNodes.length; i++){
			if(activeNodes[i] == nodeId){
				activeNodes.splice(i, 1);
				break;
			}
		}
		
		//if node has active edge, it needs to be removed from the active path as well
		for(var i=0; i<activeEdges.length; i++){
			if(activeEdges[i].v==nodeId || activeEdges[i].w==nodeId){
				//reset class of active edge (d3 and graph)
				changeEdgeToDeactive(activeEdges[i]);
				//remove deactivated edge from array activeEdges
				activeEdges.splice(i,1);
				break;
			}		
		}
		
		//if removeChildren==true: 
		//check if node has formal children 
		//and none of those children have children themselves
		//then children and all edges where children are start- or endnode
		//are removed from graph
		if(removeChildren && remove){
			removeNodeChildren(nodeId);
			d3.select("svg g").call(render, g);
			centerNode(nodeId);
		}
	}
}

function removeNodeChildren(nodeId){
				if(hasFormalChild(nodeId)){
					var allChildEdges = g.outEdges(nodeId);
					var childEdges=[];
					for(var i=0; i<allChildEdges.length; i++){
						if(isFormalEdge(allChildEdges[i])){
							childEdges.push(allChildEdges[i]);
						}
					}
					for(var i=0; i<childEdges.length; i++){
						if(hasFormalChild(childEdges[i].w)){
							removeNodeChildren(childEdges[i].w);
						}
					}
					for(var i=0; i<childEdges.length; i++){
						var removableEdges = g.nodeEdges(childEdges[i].w);
						for(var j=0; j<removableEdges.length; j++){
							g.removeEdge(removableEdges[j].v, removableEdges[j].w);
						}
						g.removeNode(childEdges[i].w);
						
					}
				//redraw graph (layout changes when nodes are removed)
				d3.select("svg g").call(render, g);
				//center deactivated node
				//otherwise user might lose orientation
				centerNode(nodeId);
				}

}

//resets the active path
function resetActivePath(){
	//reset all active nodes	
	for(var i=0;i<activeNodes.length;i++){
		changeNodeToDeactive(activeNodes[i]);
	}
	//reset all active edges
	for(var i=0; i<activeEdges.length; i++){
		changeEdgeToDeactive(activeEdges[i]);
	}
	//reset variables activeNodes and activeEdges
	activeNodes=[];
	activeEdges=[];	
}